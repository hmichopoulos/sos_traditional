CREATE TABLE activity_two
(
  id bigint NOT NULL,
  definition character varying(255),
  purpose character varying(255),
  target character varying(255),
  user_id bigint NOT NULL,
  CONSTRAINT activity_two_pkey PRIMARY KEY (id),
  CONSTRAINT activity_two_fk_user FOREIGN KEY (user_id)
  REFERENCES users (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
  OIDS=FALSE
);
