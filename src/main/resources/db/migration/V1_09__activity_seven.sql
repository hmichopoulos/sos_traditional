CREATE TABLE activity_seven
(
  id bigint NOT NULL,
  advantages character varying(255),
  alternative character varying(255),
  disadvantages character varying(255),
  help_gained character varying(255),
  hypothetical_case character varying(255),
  hypothetical_step_1 character varying(255),
  hypothetical_step_2 character varying(255),
  hypothetical_step_3 character varying(255),
  hypothetical_step_4 character varying(255),
  hypothetical_step_5 character varying(255),
  hypothetical_step_6 character varying(255),
  hypothetical_step_7 character varying(255),
  target_purpose character varying(255),
  think_alternatives character varying(255),
  user_id bigint NOT NULL,
  CONSTRAINT activity_seven_pkey PRIMARY KEY (id),
  CONSTRAINT activity_seven_fk_user FOREIGN KEY (user_id)
  REFERENCES users (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
  OIDS=FALSE
);
