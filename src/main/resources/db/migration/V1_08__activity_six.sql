CREATE TABLE activity_six
(
  id bigint NOT NULL,
  alternatives character varying(255),
  decision_need character varying(255),
  knowledge_benefit character varying(255),
  no_decision character varying(255),
  two_persons character varying(255),
  user_id bigint NOT NULL,
  CONSTRAINT activity_six_pkey PRIMARY KEY (id),
  CONSTRAINT activity_six_fk_user FOREIGN KEY (user_id)
  REFERENCES users (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
  OIDS=FALSE
);

CREATE TABLE activity_six_decisions
(
  id bigint NOT NULL,
  rating bigint,
  text character varying(255),
  CONSTRAINT activity_six_decisions_fk_activity_six FOREIGN KEY (id)
  REFERENCES activity_six (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
  OIDS=FALSE
);
