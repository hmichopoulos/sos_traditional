CREATE TABLE activity_one
(
  id bigint NOT NULL,
  activity_choice character varying(255),
  image_path character varying(255),
  choices_and_consequences character varying(255),
  decision_example character varying(255),
  decision_make_alternatives character varying(255),
  decision_make_how character varying(255),
  decision_make_pros_cons character varying(255),
  late_absent boolean,
  late_bad_mark boolean,
  late_escape boolean,
  late_nothing boolean,
  on_break_bad_note boolean,
  on_break_loose_break boolean,
  on_break_nothing boolean,
  submission_bad_mark boolean,
  submission_bad_note boolean,
  submission_nothing boolean,
  water_break_glass boolean,
  water_get_wet boolean,
  water_nothing boolean,
  water_spill boolean,
  user_id bigint NOT NULL,
  CONSTRAINT activity_one_pkey PRIMARY KEY (id),
  CONSTRAINT activity_one_fk_user FOREIGN KEY (user_id)
  REFERENCES users (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
  OIDS=FALSE
);
