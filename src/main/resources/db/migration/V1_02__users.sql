CREATE TABLE users
(
  id bigint NOT NULL,
  name character varying(255),
  password character varying(255),
  surname character varying(255),
  username character varying(255),
  CONSTRAINT users_pkey PRIMARY KEY (id),
  CONSTRAINT users_u_username UNIQUE (username)
)
  WITH (
  OIDS=FALSE
);
