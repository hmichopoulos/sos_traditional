create table persistent_logins (
  username character varying(64) not null,
  series character varying(64) primary key,
  token character varying(64) not null,
  last_used timestamp without time zone not null
);