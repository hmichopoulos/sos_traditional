CREATE TABLE activity_four
(
  id bigint NOT NULL,
  advantages character varying(255),
  become_singer character varying(255),
  decision character varying(255),
  difficulties character varying(255),
  disadvantages character varying(255),
  drugs character varying(255),
  extra_info character varying(255),
  is_it_easy character varying(255),
  no_alcohol character varying(255),
  no_kids character varying(255),
  no_marriage character varying(255),
  no_school character varying(255),
  no_university character varying(255),
  rationale character varying(255),
  step_1 character varying(255),
  step_2 character varying(255),
  step_7 character varying(255),
  validate_info character varying(255),
  user_id bigint NOT NULL,
  CONSTRAINT activity_four_pkey PRIMARY KEY (id),
  CONSTRAINT activity_four_fk_user FOREIGN KEY (user_id)
  REFERENCES users (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
  OIDS=FALSE
);

