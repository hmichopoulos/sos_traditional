CREATE TABLE activity_five
(
  id bigint NOT NULL,
  available_help character varying(255),
  feeling character varying(255),
  past_help character varying(255),
  user_id bigint NOT NULL,
  CONSTRAINT activity_five_pkey PRIMARY KEY (id),
  CONSTRAINT activity_five_fk_user FOREIGN KEY (user_id)
  REFERENCES users (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
  OIDS=FALSE
);