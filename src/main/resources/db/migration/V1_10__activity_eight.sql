CREATE TABLE activity_eight
(
  id bigint NOT NULL,
  alternative_process character varying(255),
  comment_on_beer character varying(255),
  comment_on_issue_1 character varying(255),
  comment_on_issue_2 character varying(255),
  comment_on_issue_3 character varying(255),
  comment_on_issue_4 character varying(255),
  comment_on_smoking character varying(255),
  difference character varying(255),
  feelings character varying(255),
  learning_outcome character varying(255),
  opposing_consequences character varying(255),
  possible_consequence character varying(255),
  user_id bigint NOT NULL,
  CONSTRAINT activity_eight_pkey PRIMARY KEY (id),
  CONSTRAINT activity_eight_fk_user FOREIGN KEY (user_id)
  REFERENCES users (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
  OIDS=FALSE
);

CREATE TABLE activity_eight_examples
(
  id bigint NOT NULL,
  example character varying(255),
  CONSTRAINT activity_eight_examples_fk_activity_eight FOREIGN KEY (id)
  REFERENCES activity_eight (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
  OIDS=FALSE
);