CREATE TABLE activity_three
(
  id bigint NOT NULL,
  available_help character varying(255),
  deadline timestamp without time zone,
  improvement character varying(255),
  past_help character varying(255),
  steps character varying(255),
  target character varying(255),
  user_id bigint NOT NULL,
  CONSTRAINT activity_three_pkey PRIMARY KEY (id),
  CONSTRAINT activity_three_fk_user FOREIGN KEY (user_id)
  REFERENCES users (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
  OIDS=FALSE
);

CREATE TABLE activity_three_obstacle
(
  id bigint NOT NULL,
  available_help character varying(255),
  description character varying(255),
  CONSTRAINT activity_three_obstacle_fk_activity_three FOREIGN KEY (id)
  REFERENCES activity_three (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
  OIDS=FALSE
);
