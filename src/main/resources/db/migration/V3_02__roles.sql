alter table users add column user_role character varying (64);
update users set user_role = 'USER';
alter table users alter column user_role set not null;