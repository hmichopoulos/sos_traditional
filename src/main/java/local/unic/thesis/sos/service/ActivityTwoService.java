package local.unic.thesis.sos.service;

import local.unic.thesis.sos.persistence.model.ActivityThree;
import local.unic.thesis.sos.persistence.model.ActivityTwo;
import local.unic.thesis.sos.persistence.repository.ActivityThreeRepository;
import local.unic.thesis.sos.persistence.repository.ActivityTwoRepository;
import local.unic.thesis.sos.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ActivityTwoService {

    @Autowired
    ActivityTwoRepository activityTwoRepository;

    @Autowired
    ActivityThreeRepository activityThreeRepository;

    @Autowired
    UserRepository userRepository;

    public ActivityTwo findOneOrEmpty(String username) {
        ActivityTwo activityTwo = activityTwoRepository.findOneByUserUsername(username);
        if (activityTwo == null) {
            activityTwo = new ActivityTwo();
            activityTwo.setUser(userRepository.findOneByUsername(username));
        }
        return activityTwo;
    }

    public ActivityTwo findOneOrEmpty(long userId) {
        return findOneOrEmpty(userRepository.findOne(userId).getUsername());
    }

    public List<ActivityTwo> findOthersResponses(String username) {
        return activityTwoRepository.findFirst5ByUserUsernameNot(username);
    }

    public void updateDefinitionAndPurpose(String username, String definition, String purpose) {
        ActivityTwo activityTwo = findOneOrEmpty(username);
        activityTwo.setDefinition(definition);
        activityTwo.setPurpose(purpose);
        activityTwoRepository.save(activityTwo);
    }

    @Transactional
    public void updateTarget(String username, String target) {
        ActivityTwo activityTwo = findOneOrEmpty(username);
        activityTwo.setTarget(target);
        activityTwoRepository.save(activityTwo);

        ActivityThree activityThree = activityThreeRepository.findOneByUserUsername(username);
        if (activityThree == null) {
            activityThree = new ActivityThree();
            activityThree.setUser(userRepository.findOneByUsername(username));
        }
        activityThree.setTarget(target);
        activityThreeRepository.save(activityThree);
    }

    public Page<ActivityTwo> findOthersOpinions(String username, int page) {
        return activityTwoRepository.findFirst5OtherOpinions(
                username,
                new PageRequest(page, 1)
        );
    }
}
