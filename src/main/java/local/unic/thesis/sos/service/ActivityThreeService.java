package local.unic.thesis.sos.service;

import local.unic.thesis.sos.persistence.model.ActivityThree;
import local.unic.thesis.sos.persistence.model.ActivityThreeObstacle;
import local.unic.thesis.sos.persistence.repository.ActivityThreeRepository;
import local.unic.thesis.sos.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ActivityThreeService {

    @Autowired
    ActivityThreeRepository activityThreeRepository;

    @Autowired
    UserRepository userRepository;

    public ActivityThree findOneOrEmpty(String username) {
        ActivityThree activityThree = activityThreeRepository.findOneByUserUsername(username);
        if (activityThree == null) {
            activityThree = new ActivityThree();
            activityThree.setUser(userRepository.findOneByUsername(username));
        }
        return activityThree;
    }

    public ActivityThree findOneOrEmpty(long userId) {
        return findOneOrEmpty(userRepository.findOne(userId).getUsername());
    }

    public List<ActivityThree> findOthersResponses(String username) {
        return activityThreeRepository.findFirst5ByUserUsernameNot(username);
    }

    public void addObstacle(String username, String obstacle) {
        ActivityThree activityThree = findOneOrEmpty(username);
        if (activityThree.getObstacles() == null) {
            activityThree.setObstacles(new ArrayList<>());
        }
        activityThree.getObstacles().add(new ActivityThreeObstacle(obstacle));
        activityThreeRepository.save(activityThree);
    }

    public void updateObstacles(String username, List<ActivityThreeObstacle> obstacles) {
        if (obstacles == null || obstacles.size() == 0) {
            return;
        }
        ActivityThree activityThree = findOneOrEmpty(username);
        if (activityThree.getObstacles() == null || (obstacles.size() != activityThree.getObstacles().size()) ) {
            throw new IllegalArgumentException("Wrong size");
        }
        activityThree.setObstacles(obstacles);
        activityThreeRepository.save(activityThree);
    }

    public void updateImprovement(String username, String improvement) {
        ActivityThree activityThree = findOneOrEmpty(username);
        activityThree.setImprovement(improvement);
        activityThreeRepository.save(activityThree);
    }

    public void updateHelp(String username, String availableHelp, String pastHelp) {
        ActivityThree activityThree = findOneOrEmpty(username);
        activityThree.setAvailableHelp(availableHelp);
        activityThree.setPastHelp(pastHelp);
        activityThreeRepository.save(activityThree);
    }

    public void updateContract(String username, String steps, String availableHelp, Date deadline) {
        ActivityThree activityThree = findOneOrEmpty(username);
        activityThree.setSteps(steps);
        activityThree.setAvailableHelp(availableHelp);
        activityThree.setDeadline(deadline);
        activityThreeRepository.save(activityThree);
    }

    public Page<ActivityThree> findOthersExamples(String username, int page) {
        return activityThreeRepository.findFirst5OthersExamples(username, new PageRequest(page, 1));
    }
}
