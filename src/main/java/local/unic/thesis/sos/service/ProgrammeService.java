package local.unic.thesis.sos.service;

import local.unic.thesis.sos.ui.model.Programme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProgrammeService {

    @Resource(name = "theProgrammes")
    List<Programme> theProgrammes;

    public List<Programme> programmes() {
        return theProgrammes;
    }
}
