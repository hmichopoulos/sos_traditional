package local.unic.thesis.sos.service;

import local.unic.thesis.sos.persistence.model.ActivityFive;
import local.unic.thesis.sos.persistence.repository.ActivityFiveRepository;
import local.unic.thesis.sos.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityFiveService {

    @Autowired
    ActivityFiveRepository activityFiveRepository;

    @Autowired
    UserRepository userRepository;

    public ActivityFive findOneOrEmpty(String username) {
        ActivityFive activityFive = activityFiveRepository.findOneByUserUsername(username);
        if (activityFive == null) {
            activityFive = new ActivityFive();
            activityFive.setUser(userRepository.findOneByUsername(username));
            return activityFive;
        } else {
            return activityFive;
        }
    }

    public ActivityFive findOneOrEmpty(long userId) {
        return findOneOrEmpty(userRepository.findOne(userId).getUsername());
    }

    public void updateFeeling(String username, String feeling) {
        ActivityFive activityFive = findOneOrEmpty(username);
        activityFive.setFeeling(feeling);
        activityFiveRepository.save(activityFive);
    }

    public void updateAvailableHelp(String username, String availableHelp) {
        ActivityFive activityFive = findOneOrEmpty(username);
        activityFive.setAvailableHelp(availableHelp);
        activityFiveRepository.save(activityFive);
    }

    public void updatePastHelp(String username, String pastHelp) {
        ActivityFive activityFive = findOneOrEmpty(username);
        activityFive.setPastHelp(pastHelp);
        activityFiveRepository.save(activityFive);
    }
}
