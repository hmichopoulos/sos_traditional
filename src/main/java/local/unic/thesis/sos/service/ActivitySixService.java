package local.unic.thesis.sos.service;

import local.unic.thesis.sos.persistence.model.ActivitySix;
import local.unic.thesis.sos.persistence.model.ActivitySixDecision;
import local.unic.thesis.sos.persistence.repository.ActivitySixRepository;
import local.unic.thesis.sos.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ActivitySixService {

    @Autowired
    ActivitySixRepository activitySixRepository;

    @Autowired
    UserRepository userRepository;

    public ActivitySix findOneOrEmpty(String username) {
        ActivitySix activitySix = activitySixRepository.findOneByUserUsername(username);
        if (activitySix == null) {
            activitySix = new ActivitySix();
            activitySix.setUser(userRepository.findOneByUsername(username));
            return activitySix;
        } else {
            return activitySix;
        }
    }

    public ActivitySix findOneOrEmpty(long userId) {
        return findOneOrEmpty(userRepository.findOne(userId).getUsername());
    }

    public void updateDecisionNeed(String username, String decisionNeed) {
        ActivitySix activitySix = findOneOrEmpty(username);
        activitySix.setDecisionNeed(decisionNeed);
        activitySixRepository.save(activitySix);
    }

    public void updateNoDecision(String username, String noDecision) {
        ActivitySix activitySix = findOneOrEmpty(username);
        activitySix.setNoDecision(noDecision);
        activitySixRepository.save(activitySix);
    }

    public void updateTwoPersons(String username, String twoPersons) {
        ActivitySix activitySix = findOneOrEmpty(username);
        activitySix.setTwoPersons(twoPersons);
        activitySixRepository.save(activitySix);
    }

    public void updateKnowledgeBenefit(String username, String knowledgeBenefit) {
        ActivitySix activitySix = findOneOrEmpty(username);
        activitySix.setKnowledgeBenefit(knowledgeBenefit);
        activitySixRepository.save(activitySix);
    }

    public void updateAlternatives(String username, String alternatives) {
        ActivitySix activitySix = findOneOrEmpty(username);
        activitySix.setAlternatives(alternatives);
        activitySixRepository.save(activitySix);
    }

    public void addDecision(String username, String decision) {
        ActivitySix activitySix = findOneOrEmpty(username);
        if (activitySix.getDecisions() == null) {
            activitySix.setDecisions(new ArrayList<>());
        }
        activitySix.getDecisions().add(new ActivitySixDecision(decision));
        activitySixRepository.save(activitySix);
    }

    public void updateDecisions(String username, List<ActivitySixDecision> decisions) {
        if (decisions == null || decisions.size() == 0) {
            return;
        }
        ActivitySix activitySix = findOneOrEmpty(username);
        if (activitySix.getDecisions().size() != decisions.size()) {
            throw new IllegalArgumentException("Wrong length of decisions");
        }
        activitySix.getDecisions().clear();
        activitySix.getDecisions().addAll(decisions);
        activitySixRepository.save(activitySix);
    }
}
