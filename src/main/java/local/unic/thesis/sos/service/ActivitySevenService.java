package local.unic.thesis.sos.service;

import local.unic.thesis.sos.persistence.model.ActivitySeven;
import local.unic.thesis.sos.persistence.repository.ActivitySevenRepository;
import local.unic.thesis.sos.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivitySevenService {

    @Autowired
    ActivitySevenRepository activitySevenRepository;

    @Autowired
    UserRepository userRepository;

    public ActivitySeven findOneOrEmpty(String username) {
        ActivitySeven activitySeven = activitySevenRepository.findOneByUserUsername(username);
        if (activitySeven == null) {
            activitySeven = new ActivitySeven();
            activitySeven.setUser(userRepository.findOneByUsername(username));
            return activitySeven;
        } else {
            return activitySeven;
        }
    }

    public ActivitySeven findOneOrEmpty(long userId) {
        return findOneOrEmpty(userRepository.findOne(userId).getUsername());
    }

    public List<ActivitySeven> find5OtherResponses(String username) {
        return activitySevenRepository.findFirst5ByUserUsernameNot(username);
    }

    public void updateAlternativeInfo(String username, String alternative, String advantages, String disadvantages) {
        ActivitySeven activitySeven = findOneOrEmpty(username);
        activitySeven.setAlternative(alternative);
        activitySeven.setAdvantages(advantages);
        activitySeven.setDisadvantages(disadvantages);
        activitySevenRepository.save(activitySeven);
    }


    public void updateHypotheticalScenario(String username, String hypotheticalCase, String hypotheticalStep_1, String hypotheticalStep_2, String hypotheticalStep_3, String hypotheticalStep_4, String hypotheticalStep_5, String hypotheticalStep_6, String hypotheticalStep_7) {
        ActivitySeven activitySeven = findOneOrEmpty(username);
        activitySeven.setHypotheticalCase(hypotheticalCase);
        activitySeven.setHypotheticalStep_1(hypotheticalStep_1);
        activitySeven.setHypotheticalStep_2(hypotheticalStep_2);
        activitySeven.setHypotheticalStep_3(hypotheticalStep_3);
        activitySeven.setHypotheticalStep_4(hypotheticalStep_4);
        activitySeven.setHypotheticalStep_5(hypotheticalStep_5);
        activitySeven.setHypotheticalStep_6(hypotheticalStep_6);
        activitySeven.setHypotheticalStep_7(hypotheticalStep_7);
        activitySevenRepository.save(activitySeven);
    }


    public void updateHelpGained(String username, String helpGained) {
        ActivitySeven activitySeven = findOneOrEmpty(username);
        activitySeven.setHelpGained(helpGained);
        activitySevenRepository.save(activitySeven);
    }

    public void updateThinkAlternatives(String username, String thinkAlternatives) {
        ActivitySeven activitySeven = findOneOrEmpty(username);
        activitySeven.setThinkAlternatives(thinkAlternatives);
        activitySevenRepository.save(activitySeven);
    }

    public void updateTargetPurpose(String username, String targetPurpose) {
        ActivitySeven activitySeven = findOneOrEmpty(username);
        activitySeven.setTargetPurpose(targetPurpose);
        activitySevenRepository.save(activitySeven);
    }

    public Page<ActivitySeven> findOthersAlternativeInfo(String username, int page, int pageSize) {
        return activitySevenRepository.findOthersAlternativeInfo(username, new PageRequest(page, pageSize));
    }

    public Page<ActivitySeven> findOthersHypotheticalCases(String username, int page, int pageSize) {
        return activitySevenRepository.findByUserUsernameNotAndHypotheticalCaseNotNull(username, new PageRequest(page, pageSize));
    }

    public Page<ActivitySeven> findOthersHelpGained(String username, int page, int pageSize) {
        return activitySevenRepository.findByUserUsernameNotAndHelpGainedNotNull(username, new PageRequest(page, pageSize));
    }

    public Page<ActivitySeven> findOthersThinkAlternatives(String username, int page, int pageSize) {
        return activitySevenRepository.findByUserUsernameNotAndThinkAlternativesNotNull(username, new PageRequest(page, pageSize));
    }

    public Page<ActivitySeven> findOthersTargetPurpose(String username, int page, int pageSize) {
        return activitySevenRepository.findByUserUsernameNotAndTargetPurposeNotNull(username, new PageRequest(page, pageSize));
    }
}
