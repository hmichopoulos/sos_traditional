package local.unic.thesis.sos.service;

import local.unic.thesis.sos.persistence.model.ActivityOne;
import local.unic.thesis.sos.persistence.model.ActivityOneActivity;
import local.unic.thesis.sos.persistence.repository.ActivityOneRepository;
import local.unic.thesis.sos.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityOneService {

    @Autowired
    ActivityOneRepository activityOneRepository;

    @Autowired
    UserRepository userRepository;

    public ActivityOne findOneOrEmpty(String username) {
        ActivityOne activityOne = activityOneRepository.findOneByUserUsername(username);
        if (activityOne == null) {
            activityOne = new ActivityOne();
            activityOne.setUser(userRepository.findOneByUsername(username));
        }
        if (activityOne.getActivity() == null) {
            activityOne.setActivity(new ActivityOneActivity());
        }
        return activityOne;
    }

    public ActivityOne findOneOrEmpty(long userId) {
        return findOneOrEmpty(userRepository.findOne(userId).getUsername());
    }

    public void updateWater(String username, ActivityOne me) {
        ActivityOne activityOne = findOneOrEmpty(username);
        activityOne.setWaterBreakGlass(me.getWaterBreakGlass());
        activityOne.setWaterGetWet(me.getWaterGetWet());
        activityOne.setWaterNothing(me.getWaterNothing());
        activityOne.setWaterSpill(me.getWaterSpill());
        activityOneRepository.save(activityOne);
    }

    public void updateLateForSchool(String username, ActivityOne me) {
        ActivityOne activityOne = findOneOrEmpty(username);
        activityOne.setLateAbsent(me.getLateAbsent());
        activityOne.setLateBadMark(me.getLateBadMark());
        activityOne.setLateEscape(me.getLateEscape());
        activityOne.setLateNothing(me.getLateNothing());
        activityOneRepository.save(activityOne);
    }

    public void updateSubmission(String username, ActivityOne me) {
        ActivityOne activityOne = findOneOrEmpty(username);
        activityOne.setSubmissionBadMark(me.getSubmissionBadMark());
        activityOne.setSubmissionBadNote(me.getSubmissionBadNote());
        activityOne.setSubmissionNothing(me.getSubmissionNothing());
        activityOneRepository.save(activityOne);
    }

    public void updateBreak(String username, ActivityOne me) {
        ActivityOne activityOne = findOneOrEmpty(username);
        activityOne.setOnBreakBadNote(me.getOnBreakBadNote());
        activityOne.setOnBreakLooseBreak(me.getOnBreakLooseBreak());
        activityOne.setOnBreakNothing(me.getOnBreakNothing());
        activityOneRepository.save(activityOne);
    }

    public void updateOpinion(String username, ActivityOne me) {
        ActivityOne activityOne = findOneOrEmpty(username);
        activityOne.setChoicesAndConsequences(me.getChoicesAndConsequences());
        activityOne.setDecisionExample(me.getDecisionExample());
        activityOneRepository.save(activityOne);
    }

    public void updateProcess(String username, ActivityOne me) {
        ActivityOne activityOne = findOneOrEmpty(username);
        activityOne.setDecisionMakeAlternatives(me.getDecisionMakeAlternatives());
        activityOne.setDecisionMakeHow(me.getDecisionMakeHow());
        activityOne.setDecisionMakeProsCons(me.getDecisionMakeProsCons());
        activityOneRepository.save(activityOne);
    }

    public void updateActivityChoice(String username, ActivityOneActivity activity) {
        ActivityOne activityOne = findOneOrEmpty(username);
        activityOne.getActivity().setActivityChoice(activity.getActivityChoice());
        activityOneRepository.save(activityOne);
    }

    public void updateActivityFile(String username, String filename) {
        ActivityOne activityOne = findOneOrEmpty(username);
        activityOne.getActivity().setImagePath(filename);
        activityOneRepository.save(activityOne);
    }

    public List<ActivityOne> findOthersResponses(String username) {
        return activityOneRepository.findFirst5ByUserUsernameNot(username);
    }

    public Page<ActivityOne> findOthersOpinions(String username, int page) {
        return activityOneRepository.findFirst5OtherOpinions(username, new PageRequest(page, 1));
    }

    public Page<ActivityOne> findOthersProcesses(String username, int page) {
        return activityOneRepository.findFirst5OtherProcesses(username, new PageRequest(page, 1));
    }

    public Page<ActivityOne> findOthersFiles(String username, int page) {
        return activityOneRepository.findFirst5OtherFiles(username, new PageRequest(page, 1));
    }
}
