package local.unic.thesis.sos.service;

import local.unic.thesis.sos.persistence.model.ActivityEight;
import local.unic.thesis.sos.persistence.repository.ActivityEightRepository;
import local.unic.thesis.sos.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ActivityEightService {

    @Autowired
    ActivityEightRepository activityEightRepository;

    @Autowired
    UserRepository userRepository;

    public ActivityEight findOneOrEmpty(String username) {
        ActivityEight activityEight = activityEightRepository.findOneByUserUsername(username);
        if (activityEight == null) {
            activityEight = new ActivityEight();
            activityEight.setUser(userRepository.findOneByUsername(username));
            return activityEight;
        } else {
            return activityEight;
        }
    }

    public ActivityEight findOneOrEmpty(long userId) {
        return findOneOrEmpty(userRepository.findOne(userId).getUsername());
    }

    public List<ActivityEight> find5OtherResponses(String username) {
        return activityEightRepository.findFirst5ByUserUsernameNot(username);
    }

    public void updateCommentOnSmoking(String username, String commentOnSmoking) {
        ActivityEight activityEight = findOneOrEmpty(username);
        activityEight.setCommentOnSmoking(commentOnSmoking);
        activityEightRepository.save(activityEight);
    }

    public void updateCommentOnBeer(String username, String commentOnBeer) {
        ActivityEight activityEight = findOneOrEmpty(username);
        activityEight.setCommentOnBeer(commentOnBeer);
        activityEightRepository.save(activityEight);
    }

    public List<String> getOtherApprovalSeekingExamples(String username) {
        /*List<String> list = new ArrayList<>();
        for (List<String> l : activityEightRepository.findFirst10OtherApprovalSeekingExamples(username)) {
            list.addAll(l);
        }
        return list;*/
        return activityEightRepository.findFirst10OtherApprovalSeekingExamples(username);
    }

    public void addApprovalSeekingExample(String username, String example) {
        ActivityEight activityEight = findOneOrEmpty(username);
        if (activityEight.getApprovalSeekingExamples() == null) {
            activityEight.setApprovalSeekingExamples(new ArrayList<>());
        }
        activityEight.getApprovalSeekingExamples().add(example);
        activityEightRepository.save(activityEight);
    }

    public void updateTheDifference(String username, String difference) {
        ActivityEight activityEight = findOneOrEmpty(username);
        activityEight.setDifference(difference);
        activityEightRepository.save(activityEight);
    }

    public void updatePossibleConsequence(String username, String consequence) {
        ActivityEight activityEight = findOneOrEmpty(username);
        activityEight.setPossibleConsequence(consequence);
        activityEightRepository.save(activityEight);
    }

    public void updateOpposingConsequences(String username, String opposingConsequences) {
        ActivityEight activityEight = findOneOrEmpty(username);
        activityEight.setOpposingConsequences(opposingConsequences);
        activityEightRepository.save(activityEight);
    }

    public void updateAlternativeProcess(String username, String alternative) {
        ActivityEight activityEight = findOneOrEmpty(username);
        activityEight.setAlternativeProcess(alternative);
        activityEightRepository.save(activityEight);
    }

    public void updateFeelings(String username, String feelings) {
        ActivityEight activityEight = findOneOrEmpty(username);
        activityEight.setFeelings(feelings);
        activityEightRepository.save(activityEight);
    }

    public void updateLearningOutcome(String username, String learningOutcome) {
        ActivityEight activityEight = findOneOrEmpty(username);
        activityEight.setLearningOutcome(learningOutcome);
        activityEightRepository.save(activityEight);
    }

    public void updateCommentOnIssue_1(String username, String comment) {
        ActivityEight activityEight = findOneOrEmpty(username);
        activityEight.setCommentOnIssue_1(comment);
        activityEightRepository.save(activityEight);
    }

    public void updateCommentOnIssue_2(String username, String comment) {
        ActivityEight activityEight = findOneOrEmpty(username);
        activityEight.setCommentOnIssue_2(comment);
        activityEightRepository.save(activityEight);
    }

    public void updateCommentOnIssue_3(String username, String comment) {
        ActivityEight activityEight = findOneOrEmpty(username);
        activityEight.setCommentOnIssue_3(comment);
        activityEightRepository.save(activityEight);
    }

    public void updateCommentOnIssue_4(String username, String comment) {
        ActivityEight activityEight = findOneOrEmpty(username);
        activityEight.setCommentOnIssue_4(comment);
        activityEightRepository.save(activityEight);
    }
}
