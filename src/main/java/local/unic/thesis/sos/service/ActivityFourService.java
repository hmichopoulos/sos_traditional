package local.unic.thesis.sos.service;

import local.unic.thesis.sos.persistence.model.ActivityFour;
import local.unic.thesis.sos.persistence.repository.ActivityFourRepository;
import local.unic.thesis.sos.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ActivityFourService {

    @Autowired
    ActivityFourRepository activityFourRepository;

    @Autowired
    UserRepository userRepository;

    public ActivityFour findOneOrEmpty(String username) {
        ActivityFour activityFour = activityFourRepository.findOneByUserUsername(username);
        if (activityFour == null) {
            activityFour = new ActivityFour();
            activityFour.setUser(userRepository.findOneByUsername(username));
            return activityFour;
        } else {
            return activityFour;
        }
    }

    public ActivityFour findOneOrEmpty(long userId) {
        return findOneOrEmpty(userRepository.findOne(userId).getUsername());
    }

    public void updateStep1(String username, String step_1) {
        ActivityFour activityFour = findOneOrEmpty(username);
        activityFour.setStep_1(step_1);
        activityFourRepository.save(activityFour);
    }


    public void updateStep2(String username, String step_2) {
        ActivityFour activityFour = findOneOrEmpty(username);
        activityFour.setStep_2(step_2);
        activityFourRepository.save(activityFour);
    }

    public void updateStep3(String username, String extraInfo, String validateInfo) {
        ActivityFour activityFour = findOneOrEmpty(username);
        activityFour.setExtraInfo(extraInfo);
        activityFour.setValidateInfo(validateInfo);
        activityFourRepository.save(activityFour);
    }

    public void updateStep4(String username, String noUniversity, String noMarriage, String noSchool, String drugs, String becomeSinger, String noAlcohol, String noKids, String rationale) {
        ActivityFour activityFour = findOneOrEmpty(username);
        activityFour.setNoUniversity(noUniversity);
        activityFour.setNoMarriage(noMarriage);
        activityFour.setNoSchool(noSchool);
        activityFour.setDrugs(drugs);
        activityFour.setBecomeSinger(becomeSinger);
        activityFour.setNoAlcohol(noAlcohol);
        activityFour.setNoKids(noKids);
        activityFour.setRationale(rationale);
        activityFourRepository.save(activityFour);
    }

    public void updateStep5(String username, String decision, String advantages, String disadvantages) {
        ActivityFour activityFour = findOneOrEmpty(username);
        activityFour.setDecision(decision);
        activityFour.setAdvantages(advantages);
        activityFour.setDisadvantages(disadvantages);
        activityFourRepository.save(activityFour);
    }

    public void updateStep6(String username, String isItEasy, String difficulties) {
        ActivityFour activityFour = findOneOrEmpty(username);
        activityFour.setIsItEasy(isItEasy);
        activityFour.setDifficulties(difficulties);
        activityFourRepository.save(activityFour);
    }

    public void updateStep7(String username, String step_7) {
        ActivityFour activityFour = findOneOrEmpty(username);
        activityFour.setStep_7(step_7);
        activityFourRepository.save(activityFour);
    }

    public Page<ActivityFour> findOthersStep1(String username, int page, int pageSize) {
        return activityFourRepository.findOthersStep1(username, new PageRequest(page, pageSize));
    }

    public Page<ActivityFour> findOthersStep2(String username, int page, int pageSize) {
        return activityFourRepository.findOthersStep2(username, new PageRequest(page, pageSize));
    }

    public Page<ActivityFour> findOthersStep3(String username, int page, int pageSize) {
        return activityFourRepository.findOthersStep3(username, new PageRequest(page, pageSize));
    }

    public Page<ActivityFour> findOthersStep4(String username, int page, int pageSize) {
        return activityFourRepository.findOthersStep4(username, new PageRequest(page, pageSize));
    }

    public Page<ActivityFour> findOthersStep5(String username, int page, int pageSize) {
        return activityFourRepository.findOthersStep5(username, new PageRequest(page, pageSize));
    }

    public Page<ActivityFour> findOthersStep6(String username, int page, int pageSize) {
        return activityFourRepository.findOthersStep6(username, new PageRequest(page, pageSize));
    }

    public Page<ActivityFour> findOthersStep7(String username, int page, int pageSize) {
        return activityFourRepository.findOthersStep7(username, new PageRequest(page, pageSize));
    }
}
