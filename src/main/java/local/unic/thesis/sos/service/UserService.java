package local.unic.thesis.sos.service;

import local.unic.thesis.sos.persistence.model.Role;
import local.unic.thesis.sos.persistence.model.User;
import local.unic.thesis.sos.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public void register(User user) {
        user.setRole(Role.USER);
        userRepository.save(user);
    }

    public User findOneByUsername(String username) {
        return userRepository.findOneByUsername(username);
    }

    public Page<User> findAll(int page, int size) {
        return userRepository.findAll(new PageRequest(page, size));
    }

    public User find(long id) {
        return userRepository.findOne(id);
    }
}
