package local.unic.thesis.sos.ui.controller;

import local.unic.thesis.sos.persistence.model.ActivityOne;
import local.unic.thesis.sos.persistence.model.ActivityOneActivity;
import local.unic.thesis.sos.service.ActivityOneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.Principal;
import java.util.Date;

import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.enrichModel;
import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.nextStep;

@Controller
@RequestMapping("/activity/1/")
public class ActivityOneController extends ActivityBaseController {

    @Autowired
    ActivityOneService activityOneService;

    private int currentLesson = 0;

    @Override
    public int currentLesson() {
        return currentLesson;
    }

    @RequestMapping(value = "{currentTab}-{currentStep}", method = RequestMethod.GET)
    public void s0_0(Model model, Principal principal, @PathVariable int currentTab, @PathVariable int currentStep) {
        handleGets(model, principal, currentTab, currentStep);
    }

    private String handleGets(Model model, Principal principal, int currentTab, int currentStep) {
        enrichModel(model, modules(), currentLesson, currentTab, currentStep);
        ActivityOne activityOne = activityOneService.findOneOrEmpty(principal.getName());
        model.addAttribute("me", activityOne);
        model.addAttribute("activity", activityOne.getActivity());
        model.addAttribute("others", activityOneService.findOthersResponses(principal.getName()));
        return "activity/1/" + currentTab + "-" + currentStep;
    }

    @RequestMapping(value = "1-1", method = RequestMethod.POST)
    public String s1_1Submit(
            Principal principal,
            @ModelAttribute ActivityOne me
    ) {
        activityOneService.updateWater(principal.getName(), me);
        return "redirect:" + nextStep(modules(), currentLesson, 1, 1);
    }

    @RequestMapping(value = "1-2", method = RequestMethod.POST)
    public String s1_2Submit(
            Principal principal,
            @ModelAttribute ActivityOne me
    ) {
        activityOneService.updateLateForSchool(principal.getName(), me);
        return "redirect:" + nextStep(modules(), currentLesson, 1, 2);
    }

    @RequestMapping(value = "1-3", method = RequestMethod.POST)
    public String s1_3Submit(
            Principal principal,
            @ModelAttribute ActivityOne me
    ) {
        activityOneService.updateSubmission(principal.getName(), me);
        return "redirect:" + nextStep(modules(), currentLesson, 1, 3);
    }

    @RequestMapping(value = "1-4", method = RequestMethod.POST)
    public String s1_4Submit(
            Principal principal,
            @ModelAttribute ActivityOne me
    ) {
        activityOneService.updateBreak(principal.getName(), me);
        return "redirect:" + nextStep(modules(), currentLesson, 1, 4);
    }

    @RequestMapping(value = "3-1", method = RequestMethod.POST)
    public String s3_1Submit(
            Principal principal,
            @ModelAttribute ActivityOne me
    ) {
        activityOneService.updateOpinion(principal.getName(), me);
        return "redirect:" + nextStep(modules(), currentLesson, 3, 1);
    }

    @RequestMapping(value = "3-2", method = RequestMethod.GET)
    public void s3_2(
            Model model,
            Principal principal,
            @RequestParam(required = false, defaultValue = "0") int page
    ) {
        enrichModel(model, modules(), currentLesson, 3, 2);
        ActivityOne activityOne = activityOneService.findOneOrEmpty(principal.getName());
        model.addAttribute("me", activityOne);
        model.addAttribute("activity", activityOne.getActivity());
        Page<ActivityOne> responses = activityOneService.findOthersOpinions(principal.getName(), page);
        model.addAttribute("others", responses);
        model.addAttribute("page", page);
    }

    @RequestMapping(value = "3-3", method = RequestMethod.POST)
    public String s3_3Submit(
            Principal principal,
            @ModelAttribute ActivityOne me
    ) {
        activityOneService.updateProcess(principal.getName(), me);
        return "redirect:" + nextStep(modules(), currentLesson, 3, 1);
    }

    @RequestMapping(value = "3-4", method = RequestMethod.GET)
    public void s3_4(
            Model model,
            Principal principal,
            @RequestParam(required = false, defaultValue = "0") int page
    ) {
        enrichModel(model, modules(), currentLesson, 3, 4);
        ActivityOne activityOne = activityOneService.findOneOrEmpty(principal.getName());
        model.addAttribute("me", activityOne);
        model.addAttribute("activity", activityOne.getActivity());
        Page<ActivityOne> responses = activityOneService.findOthersProcesses(principal.getName(), page);
        model.addAttribute("others", responses);
        model.addAttribute("page", page);
    }

    @RequestMapping(value = "5-1", method = RequestMethod.POST)
    public String s5_1Submit(
            Principal principal,
            @ModelAttribute ActivityOneActivity activity
            ) {
        activityOneService.updateActivityChoice(principal.getName(), activity);
        return "redirect:" + nextStep(modules(), currentLesson, 5, 1);
    }

    @RequestMapping(value = "5-2", method = RequestMethod.POST)
    public String s5_2Submit(
            Model model,
            Principal principal,
            @RequestParam("file") MultipartFile file
    ) {
        if (!file.isEmpty()) {
            try {
                File f;
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(f = new File(imageStorageLocation, "image" + (new Date().getTime()) )));
                FileCopyUtils.copy(file.getInputStream(), stream);
                stream.close();
                activityOneService.updateActivityFile(principal.getName(), f.getName());
                /*redirectAttributes.addFlashAttribute("message",
                        "You successfully uploaded " + name + "!");*/
            }
            catch (Exception e) {
                System.out.println(e);
                return handleGets(model, principal, 5, 2);
/*
                redirectAttributes.addFlashAttribute("message",
                        "You failed to upload " + name + " => " + e.getMessage());
*/
            }
            return "redirect:" + nextStep(modules(), currentLesson, 5, 2);
        } else {
            //skip 5-3
            return "redirect:" + nextStep(modules(), currentLesson, 5, 3);
        }
    }

    @RequestMapping(value = "5-3", method = RequestMethod.GET)
    public void s5_3(
            Model model,
            Principal principal,
            @RequestParam(required = false, defaultValue = "0") int page
    ) {
        enrichModel(model, modules(), currentLesson, 5, 3);
        ActivityOne activityOne = activityOneService.findOneOrEmpty(principal.getName());
        model.addAttribute("me", activityOne);
        model.addAttribute("activity", activityOne.getActivity());
        Page<ActivityOne> responses = activityOneService.findOthersFiles(principal.getName(), page);
        model.addAttribute("others", responses);
        model.addAttribute("page", page);
    }
}
