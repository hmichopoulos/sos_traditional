package local.unic.thesis.sos.ui.controller;

import local.unic.thesis.sos.service.UserService;
import local.unic.thesis.sos.ui.model.Lesson;
import local.unic.thesis.sos.ui.model.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.List;

public abstract class ActivityBaseController {

    @Resource(name = "decisionMakingLessons")
    protected List<Lesson> decisionMakingLessons;

    @Value("${app.imageLocation}")
    protected String imageStorageLocation;

    @Value("${app.requiredResponses}")
    protected boolean requiredResponses;

    protected int pageSize = 1;

    @Autowired
    UserService userService;

    @ModelAttribute("name")
    public String userName(Principal principal) {
        if (principal!=null) {
            return userService.findOneByUsername(principal.getName()).getName();
        } else {
            return null;
        }
    }

    @ModelAttribute("currentLesson")
    abstract public int currentLesson();

    protected List<Module> modules() {
        return decisionMakingLessons.get(currentLesson()).getModules();
    }

}
