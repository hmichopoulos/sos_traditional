package local.unic.thesis.sos.ui.controller;

import local.unic.thesis.sos.ui.model.Module;
import org.springframework.ui.Model;

import java.util.List;

class ActivityControllerUtil {


    static String nextStep(List<Module> activityModules, int currentLesson, int currentTab, int currentStep) {
        //next step exists
        if (activityModules.get(currentTab).getStepsCount() > currentStep + 1) {
            return "/activity/" + (currentLesson + 1) +"/" + currentTab + "-" + (currentStep+1) + ".html";
        } else {
            //next tab exists
            if (activityModules.size() > currentTab + 1) {
                return "/activity/" + (currentLesson + 1) + "/" + (currentTab + 1 )+ "-0.html";
            } else {
                return "/activity/" + (currentLesson + 2) + "/0-0.html";
            }
        }
    }

    static void enrichModel(Model model, List<Module> activityModules, int currentLesson, int currentTab, int currentStep) {
        model.addAttribute("currentTab", currentTab);
        model.addAttribute("currentStep", currentStep);
        model.addAttribute("nextStep", nextStep(activityModules, currentLesson,  currentTab, currentStep));
    }
}
