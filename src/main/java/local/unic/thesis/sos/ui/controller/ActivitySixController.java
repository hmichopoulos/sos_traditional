package local.unic.thesis.sos.ui.controller;

import local.unic.thesis.sos.service.ActivitySixService;
import local.unic.thesis.sos.ui.model.ActivitySixDecisionsForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.enrichModel;
import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.nextStep;

@Controller
@RequestMapping("/activity/6/")
public class ActivitySixController extends ActivityBaseController {

    private int currentLesson = 5;

    @Autowired
    ActivitySixService activitySixService;

    @Override
    public int currentLesson() {
        return currentLesson;
    }

    @RequestMapping(value = "{currentTab}-{currentStep}", method = RequestMethod.GET)
    public void s0_0(Model model, Principal principal, @PathVariable int currentTab, @PathVariable int currentStep) {
        handleGets(model, principal, currentTab, currentStep);
    }

    private String handleGets(Model model, Principal principal, int currentTab, int currentStep) {
        enrichModel(model, modules(), currentLesson, currentTab, currentStep);
        model.addAttribute("me", activitySixService.findOneOrEmpty(principal.getName()));
        return "/activity/6/" + currentTab + "-" + currentStep;
    }

    @RequestMapping(value = "1-1", method = RequestMethod.POST)
    public String s1_1Submit(Model model, Principal principal, @RequestParam("decision_need") String decisionNeed) {
        if (requiredResponses && StringUtils.isEmpty(decisionNeed)) {
            return handleGets(model, principal, 1, 1);
        } else {
            activitySixService.updateDecisionNeed(principal.getName(), decisionNeed);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 1);
        }
    }

    @RequestMapping(value = "1-2", method = RequestMethod.POST)
    public String s1_2Submit(Model model, Principal principal, @RequestParam("no_decision") String noDecision) {
        if (requiredResponses && StringUtils.isEmpty(noDecision)) {
            return handleGets(model, principal, 1, 2);
        } else {
            activitySixService.updateNoDecision(principal.getName(), noDecision);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 2);
        }
    }

    @RequestMapping(value = "1-3", method = RequestMethod.POST)
    public String s1_3Submit(Model model, Principal principal, @RequestParam("two_persons") String twoPersons) {
        if (requiredResponses && StringUtils.isEmpty(twoPersons)) {
            return handleGets(model, principal, 1, 3);
        } else {
            activitySixService.updateTwoPersons(principal.getName(), twoPersons);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 3);
        }
    }

    @RequestMapping(value = "1-4", method = RequestMethod.POST)
    public String s1_4Submit(Model model, Principal principal, @RequestParam("knowledge_benefit") String knowledgeBenefit) {
        if (requiredResponses && StringUtils.isEmpty(knowledgeBenefit)) {
            return handleGets(model, principal, 1, 4);
        } else {
            activitySixService.updateKnowledgeBenefit(principal.getName(), knowledgeBenefit);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 4);
        }
    }

    @RequestMapping(value = "1-5", method = RequestMethod.POST)
    public String s1_5Submit(Model model, Principal principal, @RequestParam("alternatives") String alternatives) {
        if (requiredResponses && StringUtils.isEmpty(alternatives)) {
            return handleGets(model, principal, 1, 5);
        } else {
            activitySixService.updateAlternatives(principal.getName(), alternatives);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 5);
        }
    }

    @RequestMapping(value = "2-0", method = RequestMethod.POST, params = {"add", "!next"})
    public String s2_0AddSubmit(
            Model model,
            Principal principal,
            @RequestParam("decision") String decision
    ) {
        if (StringUtils.isEmpty(decision)) {
            return handleGets(model, principal, 2, 0);
        } else {
            activitySixService.addDecision(principal.getName(), decision);
            return handleGets(model, principal, 2, 0);
        }
    }

    @RequestMapping(value = "2-0", method = RequestMethod.POST, params = {"!add", "next"})
    public String s2_0NextSubmit(
            Principal principal,
            @RequestParam("decision") String decision
    ) {
        if (!StringUtils.isEmpty(decision)) {
            activitySixService.addDecision(principal.getName(), decision);
        }
        return "redirect:" + nextStep(modules(), currentLesson, 2, 0);
    }

    @RequestMapping(value = "2-1", method = RequestMethod.POST)
    public String s2_1Submit(Principal principal, @ModelAttribute("decisions")ActivitySixDecisionsForm decisions) {
        activitySixService.updateDecisions(principal.getName(), decisions.getDecisions());
        return "redirect:" + nextStep(modules(), currentLesson, 2, 1);
    }

}
