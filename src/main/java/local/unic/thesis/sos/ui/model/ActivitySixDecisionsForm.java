package local.unic.thesis.sos.ui.model;

import local.unic.thesis.sos.persistence.model.ActivitySixDecision;

import java.util.List;

public class ActivitySixDecisionsForm {

    private List<ActivitySixDecision> decisions;

    public List<ActivitySixDecision> getDecisions() {
        return decisions;
    }

    public void setDecisions(List<ActivitySixDecision> decisions) {
        this.decisions = decisions;
    }
}
