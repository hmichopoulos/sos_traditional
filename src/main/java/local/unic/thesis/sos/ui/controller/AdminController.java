package local.unic.thesis.sos.ui.controller;

import local.unic.thesis.sos.service.*;
import local.unic.thesis.sos.ui.model.Lesson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/admin/")
public class AdminController {

    @Autowired
    UserService userService;

    @Autowired
    ActivityOneService activityOneService;

    @Autowired
    ActivityTwoService activityTwoService;

    @Autowired
    ActivityThreeService activityThreeService;

    @Autowired
    ActivityFourService activityFourService;

    @Autowired
    ActivityFiveService activityFiveService;

    @Autowired
    ActivitySixService activitySixService;

    @Autowired
    ActivitySevenService activitySevenService;

    @Autowired
    ActivityEightService activityEightService;

    @Autowired
    ActivityNineService activityNineService;

    private int userPageSize = 10;

    @Resource(name = "decisionMakingLessons")
    private List<Lesson> decisionMakingLessons;

    @ModelAttribute("lessons")
    public List<Lesson> lessons() {
        return decisionMakingLessons;
    }


    @ModelAttribute("name")
    public String name(Principal principal) {
        return userService.findOneByUsername(principal.getName()).getName();
    }

    @RequestMapping(value = "main", method = RequestMethod.GET)
    public void main(@RequestParam(required = false, defaultValue = "0") int page, Model model) {
        model.addAttribute("users", userService.findAll(page, userPageSize));
        model.addAttribute("page", page);
    }

    @RequestMapping(value = "user", method = RequestMethod.GET)
    public void user(@RequestParam long id, Model model) {
        model.addAttribute("user", userService.find(id));
    }

    @RequestMapping(value = "response/activity/1", method = RequestMethod.POST)
    public String responseOne(@RequestParam long userId, Model model) {
        model.addAttribute("response", activityOneService.findOneOrEmpty(userId));
        return "admin/responseOne :: response";
    }

    @RequestMapping(value = "response/activity/2", method = RequestMethod.POST)
    public String responseTwo(@RequestParam long userId, Model model) {
        model.addAttribute("response", activityTwoService.findOneOrEmpty(userId));
        return "admin/responseTwo :: response";
    }

    @RequestMapping(value = "response/activity/3", method = RequestMethod.POST)
    public String responseThree(@RequestParam long userId, Model model) {
        model.addAttribute("response", activityThreeService.findOneOrEmpty(userId));
        return "admin/responseThree :: response";
    }

    @RequestMapping(value = "response/activity/4", method = RequestMethod.POST)
    public String responseFour(@RequestParam long userId, Model model) {
        model.addAttribute("response", activityFourService.findOneOrEmpty(userId));
        return "admin/responseFour :: response";
    }

    @RequestMapping(value = "response/activity/5", method = RequestMethod.POST)
    public String responseFive(@RequestParam long userId, Model model) {
        model.addAttribute("response", activityFiveService.findOneOrEmpty(userId));
        return "admin/responseFive :: response";
    }

    @RequestMapping(value = "response/activity/6", method = RequestMethod.POST)
    public String responseSix(@RequestParam long userId, Model model) {
        model.addAttribute("response", activitySixService.findOneOrEmpty(userId));
        return "admin/responseSix :: response";
    }

    @RequestMapping(value = "response/activity/7", method = RequestMethod.POST)
    public String responseSeven(@RequestParam long userId, Model model) {
        model.addAttribute("response", activitySevenService.findOneOrEmpty(userId));
        return "admin/responseSeven :: response";
    }

    @RequestMapping(value = "response/activity/8", method = RequestMethod.POST)
    public String responseEigth(@RequestParam long userId, Model model) {
        model.addAttribute("response", activityEightService.findOneOrEmpty(userId));
        return "admin/responseEight :: response";
    }

    @RequestMapping(value = "response/activity/9", method = RequestMethod.POST)
    public String responseNine(@RequestParam long userId, Model model) {
        model.addAttribute("response", activityNineService.findOneOrEmpty(userId));
        return "admin/responseNine :: response";
    }
}
