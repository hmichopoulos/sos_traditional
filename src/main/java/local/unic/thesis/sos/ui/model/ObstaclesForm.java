package local.unic.thesis.sos.ui.model;

import local.unic.thesis.sos.persistence.model.ActivityThreeObstacle;

import java.util.List;

public class ObstaclesForm {

    private List<ActivityThreeObstacle> obstacles;

    public List<ActivityThreeObstacle> getObstacles() {
        return obstacles;
    }

    public void setObstacles(List<ActivityThreeObstacle> obstacles) {
        this.obstacles = obstacles;
    }

}
