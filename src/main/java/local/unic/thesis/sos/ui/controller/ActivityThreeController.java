package local.unic.thesis.sos.ui.controller;

import local.unic.thesis.sos.persistence.model.ActivityThree;
import local.unic.thesis.sos.service.ActivityThreeService;
import local.unic.thesis.sos.service.ActivityTwoService;
import local.unic.thesis.sos.ui.model.ObstaclesForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Date;

import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.enrichModel;
import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.nextStep;

@Controller
@RequestMapping("/activity/3/")
public class ActivityThreeController extends ActivityBaseController {

    private int currentLesson = 2;

    @Autowired
    ActivityThreeService activityThreeService;

    @Autowired
    ActivityTwoService activityTwoService;

    @Override
    public int currentLesson() {
        return currentLesson;
    }

    @RequestMapping(value = "{currentTab}-{currentStep}", method = RequestMethod.GET)
    public String s0_0(Model model, Principal principal, @PathVariable int currentTab, @PathVariable int currentStep) {
        if (StringUtils.isEmpty(activityTwoService.findOneOrEmpty(principal.getName()).getTarget()) ) {
            return "activity/3/no_target";
        }
        return handleGets(model, principal, currentTab, currentStep);
    }

    private String handleGets(Model model, Principal principal, int currentTab, int currentStep) {
        enrichModel(model, modules(), currentLesson, currentTab, currentStep);
        model.addAttribute("myTarget", activityTwoService.findOneOrEmpty(principal.getName()).getTarget());
        model.addAttribute("me", activityThreeService.findOneOrEmpty(principal.getName()));
        model.addAttribute("others", activityThreeService.findOthersResponses(principal.getName()));
        return "activity/3/" + currentTab + "-" + currentStep;
    }

    @RequestMapping(value = "1-0", method = RequestMethod.POST, params = {"add", "!next"})
    public String s1_0AddSubmit(
            Model model,
            Principal principal,
            @RequestParam("obstacle") String obstacle
    ) {
        if (StringUtils.isEmpty(obstacle)) {
            return handleGets(model, principal, 1, 0);
        } else {
            activityThreeService.addObstacle(principal.getName(), obstacle);
            return handleGets(model, principal, 1, 0);
        }
    }

    @RequestMapping(value = "1-0", method = RequestMethod.POST, params = {"!add", "next"})
    public String s1_0NextSubmit(
            Principal principal,
            @RequestParam(value = "obstacle", required = false) String obstacle
    ) {
        if (!StringUtils.isEmpty(obstacle)) {
            activityThreeService.addObstacle(principal.getName(), obstacle);
        }
        return "redirect:" + nextStep(modules(), currentLesson, 1, 0);
    }

    @RequestMapping(value = "1-1", method = RequestMethod.POST)
    public String s1_1Submit(Model model, Principal principal, @ModelAttribute("obstacles") ObstaclesForm obstacles) {
        activityThreeService.updateObstacles(principal.getName(), obstacles.getObstacles());
        return "redirect:" + nextStep(modules(), currentLesson, 1, 1);
    }

    @RequestMapping(value = "2-0", method = RequestMethod.POST)
    public String s2_0Submit(Model model, Principal principal, @RequestParam("improvement")String improvement) {
        if (StringUtils.isEmpty(improvement)) {
            return handleGets(model, principal, 2, 0);
        } else {
            activityThreeService.updateImprovement(principal.getName(), improvement);
            return "redirect:" + nextStep(modules(), currentLesson, 2, 0);
        }
    }

    @RequestMapping(value = "2-1", method = RequestMethod.POST)
    public String s2_1Submit(
            Model model,
            Principal principal,
            @RequestParam("available_help")String availableHelp,
            @RequestParam("past_help")String pastHelp
    ) {
        if (requiredResponses && StringUtils.isEmpty(availableHelp)) {
            return handleGets(model, principal, 2, 1);
        } else {
            activityThreeService.updateHelp(principal.getName(), availableHelp, pastHelp);
            return "redirect:" + nextStep(modules(), currentLesson, 2, 1);
        }
    }

    @RequestMapping(value = "2-2", method = RequestMethod.GET)
    public void s2_2(
            Model model,
            Principal principal,
            @RequestParam(required = false, defaultValue = "0") int page
    ) {
        enrichModel(model, modules(), currentLesson, 2, 2);
        Page<ActivityThree> responses = activityThreeService.findOthersExamples(principal.getName(), page);
        model.addAttribute("others", responses);
        model.addAttribute("page", page);
    }


    @RequestMapping(value = "3-0", method = RequestMethod.POST)
    public String s3_0Submit(
            Model model,
             Principal principal,
            @RequestParam("steps") String steps,
            @RequestParam("available_help") String availableHelp,
            @RequestParam("deadline") @DateTimeFormat(pattern = "yyyy-MM-dd") Date deadline
    ) {
        if (requiredResponses && StringUtils.isEmpty(steps)) {
            return handleGets(model, principal, 3, 0);
        } else {
            activityThreeService.updateContract(principal.getName(), steps, availableHelp, deadline);
            return "redirect:" + nextStep(modules(), currentLesson, 3, 0);
        }
    }

}
