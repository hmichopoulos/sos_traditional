package local.unic.thesis.sos.ui.controller;

import local.unic.thesis.sos.service.ActivityFiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.enrichModel;
import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.nextStep;

@Controller
@RequestMapping("/activity/5/")
public class ActivityFiveController extends ActivityBaseController {

    private int currentLesson = 4;

    @Autowired
    ActivityFiveService activityFiveService;

    @Override
    public int currentLesson() {
        return currentLesson;
    }

    @RequestMapping(value = "{currentTab}-{currentStep}", method = RequestMethod.GET)
    public void s0_0(Model model, Principal principal, @PathVariable int currentTab, @PathVariable int currentStep) {
        handleGets(model, principal, currentTab, currentStep);
    }

    private String handleGets(Model model, Principal principal, int currentTab, int currentStep) {
        enrichModel(model, modules(), currentLesson, currentTab, currentStep);
        model.addAttribute("me", activityFiveService.findOneOrEmpty(principal.getName()));
        return "activity/5/" + currentTab + "-" + currentStep;
    }

    @RequestMapping(value = "2-0", method = RequestMethod.POST)
    public String s2_0Submit(Model model, Principal principal, @RequestParam String feeling) {
        if (requiredResponses && StringUtils.isEmpty(feeling)) {
            return handleGets(model, principal, 2, 0);
        } else {
            activityFiveService.updateFeeling(principal.getName(), feeling);
            return "redirect:" + nextStep(modules(), currentLesson, 2, 0);
        }
    }

    @RequestMapping(value = "2-1", method = RequestMethod.POST)
    public String s2_1Submit(Model model, Principal principal, @RequestParam("available_help") String availableHelp) {
        if (requiredResponses && StringUtils.isEmpty(availableHelp)) {
            return handleGets(model, principal, 2, 1);
        } else {
            activityFiveService.updateAvailableHelp(principal.getName(), availableHelp);
            return "redirect:" + nextStep(modules(), currentLesson, 2, 1);
        }
    }

    @RequestMapping(value = "2-2", method = RequestMethod.POST)
    public String s2_2Submit(Model model, Principal principal, @RequestParam("past_help") String pastHelp) {
        if (requiredResponses && StringUtils.isEmpty(pastHelp)) {
            return handleGets(model, principal, 2, 2);
        } else {
            activityFiveService.updatePastHelp(principal.getName(), pastHelp);
            return "redirect:" + nextStep(modules(), currentLesson, 2, 2);
        }
    }
}
