package local.unic.thesis.sos.ui.model;

public class Module {

    private String title;
    private int idx;
    private int stepsCount;

    public Module() {
    }

    public Module(String title, int idx, int stepsCount) {
        this.title = title;
        this.idx = idx;
        this.stepsCount = stepsCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public int getStepsCount() {
        return stepsCount;
    }

    public void setStepsCount(int stepsCount) {
        this.stepsCount = stepsCount;
    }
}
