package local.unic.thesis.sos.ui.controller;

import local.unic.thesis.sos.persistence.model.Role;
import local.unic.thesis.sos.persistence.model.User;
import local.unic.thesis.sos.service.ProgrammeService;
import local.unic.thesis.sos.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
public class MainController {

    @Autowired
    ProgrammeService programmeService;

    @Autowired
    UserService userService;

    @RequestMapping({"/main", "/"})
    public String main(Model model, Principal principal) {
        model.addAttribute("programmes", programmeService.programmes());
        model.addAttribute("userLoggedIn", principal==null ? false : true);
        if (principal!=null) {
            User user = userService.findOneByUsername(principal.getName());
            model.addAttribute("name", user.getName());
            model.addAttribute("isAdmin", Role.ADMIN.equals(user.getRole()));
        }
        return "main";
    }

}
