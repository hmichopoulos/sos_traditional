package local.unic.thesis.sos.ui.controller;

import local.unic.thesis.sos.persistence.model.ActivityFour;
import local.unic.thesis.sos.service.ActivityFourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.enrichModel;
import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.nextStep;

@Controller
@RequestMapping("/activity/4/")
public class ActivityFourController extends ActivityBaseController {

    private int currentLesson = 3;

    @Override
    public int currentLesson() {
        return currentLesson;
    }

    @Autowired
    ActivityFourService activityFourService;

    @RequestMapping(value = "0-0", method = RequestMethod.GET)
    public String s0_0(Model model) {
        enrichModel(model, modules(), currentLesson, 0, 0);
        return "activity/4/0-0";
    }

    @RequestMapping(value = "1-0", method = RequestMethod.GET)
    public String s1_0(Model model) {
        enrichModel(model, modules(), currentLesson, 1, 0);
        return "activity/4/1-0";
    }

    @RequestMapping(value = "1-1", method = RequestMethod.GET)
    public String s1_1(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 1);
        model.addAttribute("step_1", activityFourService.findOneOrEmpty(principal.getName()).getStep_1());
        return "activity/4/1-1";
    }

    @RequestMapping(value = "1-1", method = RequestMethod.POST)
    public String s1_1Submit(Model model, Principal principal, @RequestParam String step_1) {
        if (requiredResponses && StringUtils.isEmpty(step_1)) {
            enrichModel(model, modules(), currentLesson, 1, 1);
            return "activity/4/1-1";
        } else {
            activityFourService.updateStep1(principal.getName(), step_1);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 1);
        }
    }

    @RequestMapping(value = "1-2", method = RequestMethod.GET)
    public void s1_2(@RequestParam(required = false, defaultValue = "0") int page, Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 2);
        model.addAttribute("page", page);
        model.addAttribute("others", activityFourService.findOthersStep1(principal.getName(), page, pageSize));
    }

    @RequestMapping(value = "1-3", method = RequestMethod.GET)
    public String s1_3(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 3);
        model.addAttribute("step_2", activityFourService.findOneOrEmpty(principal.getName()).getStep_2());
        return "activity/4/1-3";
    }

    @RequestMapping(value = "1-3", method = RequestMethod.POST)
    public String s1_3Submit(Model model, Principal principal, @RequestParam String step_2) {
        if (requiredResponses && StringUtils.isEmpty(step_2)) {
            enrichModel(model, modules(), currentLesson, 1, 2);
            return "activity/4/1-3";
        } else {
            activityFourService.updateStep2(principal.getName(), step_2);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 3);
        }
    }

    @RequestMapping(value = "1-4", method = RequestMethod.GET)
    public void s1_4(@RequestParam(required = false, defaultValue = "0") int page, Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 4);
        model.addAttribute("page", page);
        model.addAttribute("others", activityFourService.findOthersStep2(principal.getName(), page, pageSize));
    }

    @RequestMapping(value = "1-5", method = RequestMethod.GET)
    public String s1_5(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 5);
        ActivityFour activityFour = activityFourService.findOneOrEmpty(principal.getName());
        model.addAttribute("step_3_extra_info", activityFour.getExtraInfo());
        model.addAttribute("step_3_validate_info", activityFour.getValidateInfo());
        return "activity/4/1-5";
    }

    @RequestMapping(value = "1-5", method = RequestMethod.POST)
    public String s1_5Submit(
            Model model,
            Principal principal,
            @RequestParam("step_3_extra_info") String extraInfo,
            @RequestParam("step_3_validate_info") String validateInfo
    ) {
        if (requiredResponses && (StringUtils.isEmpty(extraInfo) || StringUtils.isEmpty(validateInfo))) {
            enrichModel(model, modules(), currentLesson, 1, 5);
            return "activity/4/1-5";
        } else {
            activityFourService.updateStep3(principal.getName(), extraInfo, validateInfo);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 5);
        }
    }

    @RequestMapping(value = "1-6", method = RequestMethod.GET)
    public void s1_6(@RequestParam(required = false, defaultValue = "0") int page, Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 6);
        model.addAttribute("page", page);
        model.addAttribute("others", activityFourService.findOthersStep3(principal.getName(), page, pageSize));
    }


    @RequestMapping(value = "1-7", method = RequestMethod.GET)
    public String s1_7(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 7);
        ActivityFour activityFour = activityFourService.findOneOrEmpty(principal.getName());
        model.addAttribute("step_4_no_university", activityFour.getNoUniversity());
        model.addAttribute("step_4_no_marriage", activityFour.getNoMarriage());
        model.addAttribute("step_4_no_school", activityFour.getNoSchool());
        model.addAttribute("step_4_drugs", activityFour.getDrugs());
        model.addAttribute("step_4_singer", activityFour.getBecomeSinger());
        model.addAttribute("step_4_no_alcohol", activityFour.getNoAlcohol());
        model.addAttribute("step_4_no_kids", activityFour.getNoKids());
        model.addAttribute("step_4_rationale", activityFour.getRationale());
        return "activity/4/1-7";
    }

    @RequestMapping(value = "1-7", method = RequestMethod.POST)
    public String s1_7Submit(
            Model model,
            Principal principal,
            @RequestParam("step_4_no_university") String noUniversity,
            @RequestParam("step_4_no_marriage") String noMarriage,
            @RequestParam("step_4_no_school") String noSchool,
            @RequestParam("step_4_drugs") String drugs,
            @RequestParam("step_4_singer") String becomeSinger,
            @RequestParam("step_4_no_alcohol") String noAlcohol,
            @RequestParam("step_4_no_kids") String noKids,
            @RequestParam("step_4_rationale") String rationale
    ) {
        if (requiredResponses && (StringUtils.isEmpty(noUniversity) || StringUtils.isEmpty(noMarriage))) {
            enrichModel(model, modules(), currentLesson, 1, 7);
            return "activity/4/1-7";
        } else {
            activityFourService.updateStep4(principal.getName(), noUniversity, noMarriage, noSchool, drugs, becomeSinger, noAlcohol, noKids, rationale);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 7);
        }
    }

    @RequestMapping(value = "1-8", method = RequestMethod.GET)
    public void s1_8(@RequestParam(required = false, defaultValue = "0") int page, Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 8);
        model.addAttribute("page", page);
        model.addAttribute("others", activityFourService.findOthersStep4(principal.getName(), page, pageSize));
    }


    @RequestMapping(value = "1-9", method = RequestMethod.GET)
    public String s1_9(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 9);
        ActivityFour activityFour = activityFourService.findOneOrEmpty(principal.getName());
        model.addAttribute("step_5_decision", activityFour.getDecision());
        model.addAttribute("step_5_advantages", activityFour.getAdvantages());
        model.addAttribute("step_5_disadvantages", activityFour.getDisadvantages());
        return "activity/4/1-9";
    }

    @RequestMapping(value = "1-9", method = RequestMethod.POST)
    public String s1_9Submit(
            Model model,
            Principal principal,
            @RequestParam("step_5_decision") String decision,
            @RequestParam("step_5_advantages") String advantages,
            @RequestParam("step_5_disadvantages") String disadvantages
    ) {
        if (requiredResponses && (StringUtils.isEmpty(decision) || StringUtils.isEmpty(advantages))) {
            enrichModel(model, modules(), currentLesson, 1, 9);
            return "activity/4/1-9";
        } else {
            activityFourService.updateStep5(principal.getName(), decision, advantages, disadvantages);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 9);
        }
    }

    @RequestMapping(value = "1-10", method = RequestMethod.GET)
    public void s1_10(@RequestParam(required = false, defaultValue = "0") int page, Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 10);
        model.addAttribute("page", page);
        model.addAttribute("others", activityFourService.findOthersStep5(principal.getName(), page, pageSize));
    }

    @RequestMapping(value = "1-11", method = RequestMethod.GET)
    public String s1_11(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 11);
        ActivityFour activityFour = activityFourService.findOneOrEmpty(principal.getName());
        model.addAttribute("step_6_is_it_easy", activityFour.getIsItEasy());
        model.addAttribute("step_6_difficulties", activityFour.getDifficulties());
        return "activity/4/1-11";
    }

    @RequestMapping(value = "1-11", method = RequestMethod.POST)
    public String s1_11Submit(
            Model model,
            Principal principal,
            @RequestParam("step_6_is_it_easy") String isItEasy,
            @RequestParam("step_6_difficulties") String difficulties
    ) {
        if (requiredResponses && (StringUtils.isEmpty(isItEasy) || StringUtils.isEmpty(difficulties))) {
            enrichModel(model, modules(), currentLesson, 1, 11);
            return "activity/4/1-11";
        } else {
            activityFourService.updateStep6(principal.getName(), isItEasy, difficulties);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 11);
        }
    }

    @RequestMapping(value = "1-12", method = RequestMethod.GET)
    public void s1_12(@RequestParam(required = false, defaultValue = "0") int page, Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 12);
        model.addAttribute("page", page);
        model.addAttribute("others", activityFourService.findOthersStep6(principal.getName(), page, pageSize));
    }

    @RequestMapping(value = "1-13", method = RequestMethod.GET)
    public String s1_13(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 13);
        model.addAttribute("step_7", activityFourService.findOneOrEmpty(principal.getName()).getStep_7());
        return "activity/4/1-13";
    }

    @RequestMapping(value = "1-13", method = RequestMethod.POST)
    public String s1_13Submit(
            Model model,
            Principal principal,
            @RequestParam("step_7") String step
    ) {
        if (requiredResponses && StringUtils.isEmpty(step)) {
            enrichModel(model, modules(), currentLesson, 1, 7);
            return "activity/4/1-13";
        } else {
            activityFourService.updateStep7(principal.getName(), step);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 13);
        }
    }

    @RequestMapping(value = "1-14", method = RequestMethod.GET)
    public void s1_14(@RequestParam(required = false, defaultValue = "0") int page, Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 14);
        model.addAttribute("page", page);
        model.addAttribute("others", activityFourService.findOthersStep7(principal.getName(), page, pageSize));
    }

    @RequestMapping(value = "1-15", method = RequestMethod.GET)
    public String s1_15(Model model) {
        enrichModel(model, modules(), currentLesson, 1, 15);
        return "redirect:" + nextStep(modules(), currentLesson, 1, 15);
    }
}
