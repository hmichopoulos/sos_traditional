package local.unic.thesis.sos.ui.model;

import java.util.ArrayList;
import java.util.List;

public class Lesson {

    private String title;
    private String link;
    private List<Module> modules = new ArrayList<>();

    public Lesson() {
    }

    public Lesson(String title, String link) {
        this.title = title;
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<Module> getModules() {
        return modules;
    }

    public void setModules(List<Module> modules) {
        this.modules = modules;
    }
}
