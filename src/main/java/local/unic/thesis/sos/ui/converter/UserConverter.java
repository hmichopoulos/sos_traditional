package local.unic.thesis.sos.ui.converter;

import local.unic.thesis.sos.persistence.model.User;
import local.unic.thesis.sos.ui.model.UserRegistration;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {

    public User convert(UserRegistration userRegistration) {
        User user = new User();
        user.setName(userRegistration.getName());
        user.setUsername(userRegistration.getUsername());
        user.setPassword(userRegistration.getPassword());
        user.setSex(userRegistration.getSex());
        return user;
    }
}
