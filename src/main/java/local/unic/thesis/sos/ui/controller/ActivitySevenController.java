package local.unic.thesis.sos.ui.controller;

import local.unic.thesis.sos.service.ActivitySevenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.enrichModel;
import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.nextStep;

@Controller
@RequestMapping("/activity/7/")
public class ActivitySevenController extends ActivityBaseController {

    private int currentLesson = 6;

    @Autowired
    ActivitySevenService activitySevenService;

    @Override
    public int currentLesson() {
        return currentLesson;
    }

    @RequestMapping(value = "{currentTab}-{currentStep}", method = RequestMethod.GET)
    public void s0_0(Model model, Principal principal, @PathVariable int currentTab, @PathVariable int currentStep) {
        handleGets(model, principal, currentTab, currentStep);
    }

    private String handleGets(Model model, Principal principal, int currentTab, int currentStep) {
        enrichModel(model, modules(), currentLesson, currentTab, currentStep);
        model.addAttribute("me", activitySevenService.findOneOrEmpty(principal.getName()));
        model.addAttribute("others", activitySevenService.find5OtherResponses(principal.getName()));
        return "/activity/7/" + currentTab + "-" + currentStep;
    }

    @RequestMapping(value = "1-1", method = RequestMethod.POST)
    public String s1_1Submit(
            Model model,
            Principal principal,
            @RequestParam("alternative") String alternative,
            @RequestParam("advantages") String advantages,
            @RequestParam("disadvantages") String disadvantages
    ) {
        if (requiredResponses && StringUtils.isEmpty(alternative)) {
            return handleGets(model, principal, 1, 1);
        } else {
            activitySevenService.updateAlternativeInfo(principal.getName(), alternative, advantages, disadvantages);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 1);
        }
    }

    @RequestMapping(value = "1-2", method = RequestMethod.GET)
    public void s1_2(
            Model model,
            Principal principal,
            @RequestParam(required = false, defaultValue = "0") int page
    ) {
        enrichModel(model, modules(), currentLesson, 1, 2);
        model.addAttribute("others", activitySevenService.findOthersAlternativeInfo(principal.getName(), page, pageSize));
        model.addAttribute("page", page);
    }

    @RequestMapping(value = "2-0", method = RequestMethod.POST)
    public String s2_0Submit(
            Model model,
            Principal principal,
            @RequestParam("hypothetical_case") String hypotheticalCase,
            @RequestParam("hc_step_1") String hypotheticalStep_1,
            @RequestParam("hc_step_2") String hypotheticalStep_2,
            @RequestParam("hc_step_3") String hypotheticalStep_3,
            @RequestParam("hc_step_4") String hypotheticalStep_4,
            @RequestParam("hc_step_5") String hypotheticalStep_5,
            @RequestParam("hc_step_6") String hypotheticalStep_6,
            @RequestParam("hc_step_7") String hypotheticalStep_7
    ) {
        if (requiredResponses && StringUtils.isEmpty(hypotheticalCase)) {
            return handleGets(model, principal, 2, 0);
        } else {
            activitySevenService.updateHypotheticalScenario(
                    principal.getName(),
                    hypotheticalCase,
                    hypotheticalStep_1,
                    hypotheticalStep_2,
                    hypotheticalStep_3,
                    hypotheticalStep_4,
                    hypotheticalStep_5,
                    hypotheticalStep_6,
                    hypotheticalStep_7
            );
            return "redirect:" + nextStep(modules(), currentLesson, 2, 0);
        }
    }

    @RequestMapping(value = "2-1", method = RequestMethod.GET)
    public void s2_1(
            Model model,
            Principal principal,
            @RequestParam(required = false, defaultValue = "0") int page
    ) {
        enrichModel(model, modules(), currentLesson, 2, 1);
        model.addAttribute("others", activitySevenService.findOthersHypotheticalCases(principal.getName(), page, pageSize));
        model.addAttribute("page", page);
    }

    @RequestMapping(value = "3-1", method = RequestMethod.POST)
    public String s3_1Submit(Model model, Principal principal, @RequestParam("help_gained") String helpGained) {
        if (requiredResponses && StringUtils.isEmpty(helpGained)) {
            return handleGets(model, principal, 3, 1);
        } else {
            activitySevenService.updateHelpGained(principal.getName(), helpGained);
            return "redirect:" + nextStep(modules(), currentLesson, 3, 1);
        }
    }

    @RequestMapping(value = "3-2", method = RequestMethod.GET)
    public void s3_2(
            Model model,
            Principal principal,
            @RequestParam(required = false, defaultValue = "0") int page
    ) {
        enrichModel(model, modules(), currentLesson, 3, 2);
        model.addAttribute("others", activitySevenService.findOthersHelpGained(principal.getName(), page, 5));
        model.addAttribute("page", page);
    }

    @RequestMapping(value = "3-3", method = RequestMethod.POST)
    public String s3_3Submit(Model model, Principal principal, @RequestParam("think_alternatives") String thinkAlternatives) {
        if (requiredResponses && StringUtils.isEmpty(thinkAlternatives)) {
            return handleGets(model, principal, 3, 3);
        } else {
            activitySevenService.updateThinkAlternatives(principal.getName(), thinkAlternatives);
            return "redirect:" + nextStep(modules(), currentLesson, 3, 3);
        }
    }

    @RequestMapping(value = "3-4", method = RequestMethod.GET)
    public void s3_4(
            Model model,
            Principal principal,
            @RequestParam(required = false, defaultValue = "0") int page
    ) {
        enrichModel(model, modules(), currentLesson, 3, 4);
        model.addAttribute("others", activitySevenService.findOthersThinkAlternatives(principal.getName(), page, 5));
        model.addAttribute("page", page);
    }

    @RequestMapping(value = "3-5", method = RequestMethod.POST)
    public String s3_5Submit(Model model, Principal principal, @RequestParam("target_purpose") String targetPurpose) {
        if (requiredResponses && StringUtils.isEmpty(targetPurpose)) {
            return handleGets(model, principal, 3, 5);
        } else {
            activitySevenService.updateTargetPurpose(principal.getName(), targetPurpose);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 5);
        }
    }

    @RequestMapping(value = "3-6", method = RequestMethod.GET)
    public void s3_6(
            Model model,
            Principal principal,
            @RequestParam(required = false, defaultValue = "0") int page
    ) {
        enrichModel(model, modules(), currentLesson, 3, 6);
        model.addAttribute("others", activitySevenService.findOthersTargetPurpose(principal.getName(), page, 5));
        model.addAttribute("page", page);
    }

}
