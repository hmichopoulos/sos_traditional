package local.unic.thesis.sos.ui.controller;

import local.unic.thesis.sos.service.UserService;
import local.unic.thesis.sos.ui.converter.UserConverter;
import local.unic.thesis.sos.ui.model.UserRegistration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.thymeleaf.util.StringUtils;

@Controller
public class LoginController {

    @Autowired
    UserService userService;

    @Autowired
    UserConverter userConverter;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(Model model) {
        model.addAttribute("userRegistration", new UserRegistration());
        return "registration_form";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@ModelAttribute UserRegistration userRegistration, Model model, BindingResult bindingResult) {
        if (!StringUtils.equals(userRegistration.getPassword(), userRegistration.getPasswordVerification())) {
            bindingResult.addError(new FieldError("userRegistration", "passwordVerification", "Οι κωδικοί δεν είναι ίδιοι!"));
            return "registration_form";
        } else {
            userService.register(userConverter.convert(userRegistration));
            return "redirect:/";
        }
    }
}
