package local.unic.thesis.sos.ui.controller;

import local.unic.thesis.sos.persistence.model.ActivityEight;
import local.unic.thesis.sos.service.ActivityEightService;
import local.unic.thesis.sos.service.UserService;
import local.unic.thesis.sos.ui.model.Lesson;
import local.unic.thesis.sos.ui.model.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.List;

import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.enrichModel;
import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.nextStep;

@Controller
@RequestMapping("/activity/8/")
public class ActivityEightController extends ActivityBaseController {

    private int currentLesson = 7;

    @Autowired
    ActivityEightService activityEightService;

    @Override
    public int currentLesson() {
        return currentLesson;
    }

    @RequestMapping(value = "0-0", method = RequestMethod.GET)
    public String s0_0(Model model) {
        enrichModel(model, modules(), currentLesson, 0, 0);
        return "activity/8/0-0";
    }

    @RequestMapping(value = "1-0", method = RequestMethod.GET)
    public String s1_0(Model model) {
        enrichModel(model, modules(), currentLesson, 1, 0);
        return "activity/8/1-0";
    }

    @RequestMapping(value = "1-1", method = RequestMethod.GET)
    public String s1_1(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 1);
        model.addAttribute("comment_on_smoking", activityEightService.findOneOrEmpty(principal.getName()).getCommentOnSmoking());
        model.addAttribute("others", activityEightService.find5OtherResponses(principal.getName()));
        return "activity/8/1-1";
    }

    @RequestMapping(value = "1-1", method = RequestMethod.POST)
    public String s1_1Submit(Model model, Principal principal, @RequestParam("comment_on_smoking") String commentOnSmoking) {
        if (requiredResponses && StringUtils.isEmpty(commentOnSmoking)) {
            return s1_1(model, principal);
        } else {
            activityEightService.updateCommentOnSmoking(principal.getName(), commentOnSmoking);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 1);
        }
    }

    @RequestMapping(value = "1-2", method = RequestMethod.GET)
    public String s1_2(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 2);
        model.addAttribute("comment_on_beer", activityEightService.findOneOrEmpty(principal.getName()).getCommentOnBeer());
        model.addAttribute("others", activityEightService.find5OtherResponses(principal.getName()));
        return "activity/8/1-2";
    }

    @RequestMapping(value = "1-2", method = RequestMethod.POST)
    public String s1_2Submit(Model model, Principal principal, @RequestParam("comment_on_beer") String commentOnBeer) {
        if (requiredResponses && StringUtils.isEmpty(commentOnBeer)) {
            return s1_2(model, principal);
        } else {
            activityEightService.updateCommentOnBeer(principal.getName(), commentOnBeer);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 2);
        }
    }

    @RequestMapping(value = "1-3", method = RequestMethod.GET)
    public String s1_3(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 1, 3);
        ActivityEight activityEight = activityEightService.findOneOrEmpty(principal.getName());
        model.addAttribute("own_examples", activityEight.getApprovalSeekingExamples());
        model.addAttribute("others_examples", activityEightService.getOtherApprovalSeekingExamples(principal.getName()));
        return "/activity/8/1-3";
    }

    @RequestMapping(value = "1-3", method = RequestMethod.POST, params = {"add", "!next"})
    public String s1_3AddSubmit(
            Model model,
            Principal principal,
            @RequestParam("example") String example
    ) {
        if (StringUtils.isEmpty(example)) {
            return s1_3(model, principal);
        } else {
            activityEightService.addApprovalSeekingExample(principal.getName(), example);
            return s1_3(model, principal);
        }
    }

    @RequestMapping(value = "1-3", method = RequestMethod.POST, params = {"!add", "next"})
    public String s1_3NextSubmit(
            Model model,
            Principal principal,
            @RequestParam("example") String example
    ) {
        if (!StringUtils.isEmpty(example)) {
            activityEightService.addApprovalSeekingExample(principal.getName(), example);
        }
        return "redirect:" + nextStep(modules(), currentLesson, 1, 3);
    }

    @RequestMapping(value = "2-0", method = RequestMethod.GET)
    public String s2_0(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 2, 0);
        model.addAttribute("difference", activityEightService.findOneOrEmpty(principal.getName()).getDifference());
        model.addAttribute("others", activityEightService.find5OtherResponses(principal.getName()));
        return "activity/8/2-0";
    }

    @RequestMapping(value = "2-0", method = RequestMethod.POST)
    public String s2_0Submit(
            Model model,
            Principal principal,
            @RequestParam("difference") String difference
    ) {
        if (requiredResponses && StringUtils.isEmpty(difference)) {
            return s2_0(model, principal);
        } else {
            activityEightService.updateTheDifference(principal.getName(), difference);
            return "redirect:" + nextStep(modules(), currentLesson, 2, 0);
        }
    }

    @RequestMapping(value = "2-1", method = RequestMethod.GET)
    public String s2_1(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 2, 1);
        model.addAttribute("consequence", activityEightService.findOneOrEmpty(principal.getName()).getPossibleConsequence());
        model.addAttribute("others", activityEightService.find5OtherResponses(principal.getName()));
        return "/activity/8/2-1";
    }

    @RequestMapping(value = "2-1", method = RequestMethod.POST)
    public String s2_1Submit(
            Model model,
            Principal principal,
            @RequestParam("consequence") String consequence
    ) {
        if (requiredResponses && StringUtils.isEmpty(consequence)) {
            return s1_2(model, principal);
        } else {
            activityEightService.updatePossibleConsequence(principal.getName(), consequence);
            return "redirect:" + nextStep(modules(), currentLesson, 2, 1);
        }
    }

    @RequestMapping(value = "2-2", method = RequestMethod.GET)
    public String s2_2(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 2, 2);
        model.addAttribute("opposing_consequences", activityEightService.findOneOrEmpty(principal.getName()).getOpposingConsequences());
        model.addAttribute("others", activityEightService.find5OtherResponses(principal.getName()));
        return "/activity/8/2-2";
    }

    @RequestMapping(value = "2-2", method = RequestMethod.POST)
    public String s2_2Submit(
            Model model,
            Principal principal,
            @RequestParam("opposing_consequences") String opposingConsequences
    ) {
        if (requiredResponses && StringUtils.isEmpty(opposingConsequences)) {
            return s2_2(model, principal);
        } else {
            activityEightService.updateOpposingConsequences(principal.getName(), opposingConsequences);
            return "redirect:" + nextStep(modules(), currentLesson, 2, 2);
        }
    }

    @RequestMapping(value = "2-3", method = RequestMethod.GET)
    public String s2_3(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 2, 3);
        model.addAttribute("alternative", activityEightService.findOneOrEmpty(principal.getName()).getAlternativeProcess());
        model.addAttribute("others", activityEightService.find5OtherResponses(principal.getName()));
        return "/activity/8/2-3";
    }

    @RequestMapping(value = "2-3", method = RequestMethod.POST)
    public String s2_3Submit(
            Model model,
            Principal principal,
            @RequestParam("alternative") String alternative
    ) {
        if (requiredResponses && StringUtils.isEmpty(alternative)) {
            return s2_3(model, principal);
        } else {
            activityEightService.updateAlternativeProcess(principal.getName(), alternative);
            return "redirect:" + nextStep(modules(), currentLesson, 2, 3);
        }
    }

    @RequestMapping(value = "2-4", method = RequestMethod.GET)
    public String s2_4(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 2, 4);
        model.addAttribute("feelings", activityEightService.findOneOrEmpty(principal.getName()).getFeelings());
        model.addAttribute("others", activityEightService.find5OtherResponses(principal.getName()));
        return "/activity/8/2-4";
    }

    @RequestMapping(value = "2-4", method = RequestMethod.POST)
    public String s2_4Submit(
            Model model,
            Principal principal,
            @RequestParam("feelings") String feelings
    ) {
        if (requiredResponses && StringUtils.isEmpty(feelings)) {
            return s2_4(model, principal);
        } else {
            activityEightService.updateFeelings(principal.getName(), feelings);
            return "redirect:" + nextStep(modules(), currentLesson, 2, 4);
        }
    }

    @RequestMapping(value = "2-5", method = RequestMethod.GET)
    public String s2_5(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 2, 5);
        model.addAttribute("learning_outcome", activityEightService.findOneOrEmpty(principal.getName()).getLearningOutcome());
        model.addAttribute("others", activityEightService.find5OtherResponses(principal.getName()));
        return "/activity/8/2-5";
    }

    @RequestMapping(value = "2-5", method = RequestMethod.POST)
    public String s2_5Submit(
            Model model,
            Principal principal,
            @RequestParam("learning_outcome") String learningOutcome
    ) {
        if (requiredResponses && StringUtils.isEmpty(learningOutcome)) {
            return s2_5(model, principal);
        } else {
            activityEightService.updateLearningOutcome(principal.getName(), learningOutcome);
            return "redirect:" + nextStep(modules(), currentLesson, 2, 5);
        }
    }

    @RequestMapping(value = "3-0", method = RequestMethod.GET)
    public String s3_0(Model model) {
        enrichModel(model, modules(), currentLesson, 3, 0);
        return "/activity/8/3-0";
    }

    @RequestMapping(value = "3-1", method = RequestMethod.GET)
    public String s3_1(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 3, 1);
        model.addAttribute("issue_1", activityEightService.findOneOrEmpty(principal.getName()).getCommentOnIssue_1());
        model.addAttribute("others", activityEightService.find5OtherResponses(principal.getName()));
        return "/activity/8/3-1";
    }

    @RequestMapping(value = "3-1", method = RequestMethod.POST)
    public String s3_1Submit(
            Model model,
            Principal principal,
            @RequestParam("issue_1") String comment
    ) {
        if (requiredResponses && StringUtils.isEmpty(comment)) {
            return s3_1(model, principal);
        } else {
            activityEightService.updateCommentOnIssue_1(principal.getName(), comment);
            return "redirect:" + nextStep(modules(), currentLesson, 3, 1);
        }
    }

    @RequestMapping(value = "3-2", method = RequestMethod.GET)
    public String s3_2(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 3, 2);
        model.addAttribute("issue_2", activityEightService.findOneOrEmpty(principal.getName()).getCommentOnIssue_2());
        model.addAttribute("others", activityEightService.find5OtherResponses(principal.getName()));
        return "/activity/8/3-2";
    }

    @RequestMapping(value = "3-2", method = RequestMethod.POST)
    public String s3_2Submit(
            Model model,
            Principal principal,
            @RequestParam("issue_2") String comment
    ) {
        if (requiredResponses && StringUtils.isEmpty(comment)) {
            return s3_2(model, principal);
        } else {
            activityEightService.updateCommentOnIssue_2(principal.getName(), comment);
            return "redirect:" + nextStep(modules(), currentLesson, 3, 2);
        }
    }

    @RequestMapping(value = "3-3", method = RequestMethod.GET)
    public String s3_3(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 3, 3);
        model.addAttribute("issue_3", activityEightService.findOneOrEmpty(principal.getName()).getCommentOnIssue_3());
        model.addAttribute("others", activityEightService.find5OtherResponses(principal.getName()));
        return "/activity/8/3-3";
    }

    @RequestMapping(value = "3-3", method = RequestMethod.POST)
    public String s3_3Submit(
            Model model,
            Principal principal,
            @RequestParam("issue_3") String comment
    ) {
        if (requiredResponses && StringUtils.isEmpty(comment)) {
            return s3_3(model, principal);
        } else {
            activityEightService.updateCommentOnIssue_3(principal.getName(), comment);
            return "redirect:" + nextStep(modules(), currentLesson, 3, 3);
        }
    }

    @RequestMapping(value = "3-4", method = RequestMethod.GET)
    public String s3_4(Model model, Principal principal) {
        enrichModel(model, modules(), currentLesson, 3, 4);
        model.addAttribute("issue_4", activityEightService.findOneOrEmpty(principal.getName()).getCommentOnIssue_4());
        model.addAttribute("others", activityEightService.find5OtherResponses(principal.getName()));
        return "/activity/8/3-4";
    }

    @RequestMapping(value = "3-4", method = RequestMethod.POST)
    public String s3_4Submit(
            Model model,
            Principal principal,
            @RequestParam("issue_4") String comment
    ) {
        if (requiredResponses && StringUtils.isEmpty(comment)) {
            return s3_4(model, principal);
        } else {
            activityEightService.updateCommentOnIssue_4(principal.getName(), comment);
            return "redirect:" + nextStep(modules(), currentLesson, 3, 4);
        }
    }
}
