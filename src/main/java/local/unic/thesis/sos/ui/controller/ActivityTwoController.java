package local.unic.thesis.sos.ui.controller;

import local.unic.thesis.sos.persistence.model.ActivityTwo;
import local.unic.thesis.sos.service.ActivityTwoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.enrichModel;
import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.nextStep;

@Controller
@RequestMapping("/activity/2/")
public class ActivityTwoController extends ActivityBaseController {

    private int currentLesson = 1;

    @Autowired
    ActivityTwoService activityTwoService;

    @Override
    public int currentLesson() {
        return currentLesson;
    }

    @ModelAttribute("name")
    public String userName(Principal principal) {
        if (principal!=null) {
            return userService.findOneByUsername(principal.getName()).getName();
        } else {
            return null;
        }
    }

    @RequestMapping(value = "{currentTab}-{currentStep}", method = RequestMethod.GET)
    public void s0_0(Model model, Principal principal, @PathVariable int currentTab, @PathVariable int currentStep) {
        handleGets(model, principal, currentTab, currentStep);
    }

    private String handleGets(Model model, Principal principal, int currentTab, int currentStep) {
        enrichModel(model, modules(), currentLesson, currentTab, currentStep);
        model.addAttribute("me", activityTwoService.findOneOrEmpty(principal.getName()));
        model.addAttribute("others", activityTwoService.findOthersResponses(principal.getName()));
        return "activity/2/" + currentTab + "-" + currentStep;
    }

    @RequestMapping(value = "1-0", method = RequestMethod.POST)
    public String s1_0Submit(
            Model model,
            Principal principal,
            @RequestParam String definition,
            @RequestParam String purpose
    ) {
        if (requiredResponses && StringUtils.isEmpty(definition)) {
            return handleGets(model, principal, 1, 0);
        } else {
            activityTwoService.updateDefinitionAndPurpose(principal.getName(), definition, purpose);
            return "redirect:" + nextStep(modules(), currentLesson, 1, 0);
        }
    }

    @RequestMapping(value = "1-1", method = RequestMethod.GET)
    public void s1_1(
            Model model,
            Principal principal,
            @RequestParam(required = false, defaultValue = "0") int page
    ) {
        enrichModel(model, modules(), currentLesson, 1, 1);        Page<ActivityTwo> responses = activityTwoService.findOthersOpinions(principal.getName(), page);
        model.addAttribute("others", responses);
        model.addAttribute("page", page);
    }

    @RequestMapping(value = "2-1", method = RequestMethod.POST)
    public String s2_1Submit(
            Model model,
            Principal principal,
            @RequestParam String target
    ) {
        if (requiredResponses && StringUtils.isEmpty(target)) {
            return handleGets(model, principal, 2, 1);
        } else {
            activityTwoService.updateTarget(principal.getName(), target);
            return "redirect:" + nextStep(modules(), currentLesson, 2, 1);
        }
    }
}
