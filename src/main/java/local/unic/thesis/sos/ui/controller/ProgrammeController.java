package local.unic.thesis.sos.ui.controller;

import local.unic.thesis.sos.ui.model.Lesson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class ProgrammeController {

    @Resource(name = "decisionMakingLessons")
    private List<Lesson> decisionMakingLessons;

    @ModelAttribute("lessons")
    public List<Lesson> lessons() {
        return decisionMakingLessons;
    }

    @RequestMapping("/decision-making")
    public void decisionMakingIndex() {
    }
}
