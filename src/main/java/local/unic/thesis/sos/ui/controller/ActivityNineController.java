package local.unic.thesis.sos.ui.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

import static local.unic.thesis.sos.ui.controller.ActivityControllerUtil.enrichModel;

@Controller
@RequestMapping("/activity/9/")
public class ActivityNineController extends ActivityBaseController {

    private int currentLesson = 8;

    @Override
    public int currentLesson() {
        return currentLesson;
    }

    @RequestMapping(value = "{currentTab}-{currentStep}", method = RequestMethod.GET)
    public void s0_0(Model model, Principal principal, @PathVariable int currentTab, @PathVariable int currentStep) {
        handleGets(model, principal, currentTab, currentStep);
    }

    private String handleGets(Model model, Principal principal, int currentTab, int currentStep) {
        enrichModel(model, modules(), currentLesson, currentTab, currentStep);
        return "activity/9/" + currentTab + "-" + currentStep;
    }
}
