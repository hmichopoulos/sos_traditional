package local.unic.thesis.sos.persistence.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ActivityEight extends AbstractActivity implements Serializable {

    @Column
    private String commentOnSmoking;

    @Column
    private String commentOnBeer;

    @ElementCollection
    @CollectionTable(name="activity_eight_examples", joinColumns=@JoinColumn(name="id"))
    @Column(name="example")
    private List<String> approvalSeekingExamples = new ArrayList<>();

    @Column
    private String difference;

    @Column
    private String opposingConsequences;

    @Column
    private String possibleConsequence;

    @Column
    private String alternativeProcess;

    @Column
    private String feelings;

    @Column
    private String learningOutcome;

    @Column
    private String commentOnIssue_1;

    @Column
    private String commentOnIssue_2;

    @Column
    private String commentOnIssue_3;

    @Column
    private String commentOnIssue_4;

    public String getCommentOnSmoking() {
        return commentOnSmoking;
    }

    public void setCommentOnSmoking(String commentOnSmoking) {
        this.commentOnSmoking = commentOnSmoking;
    }

    public String getCommentOnBeer() {
        return commentOnBeer;
    }

    public void setCommentOnBeer(String commentOnBeer) {
        this.commentOnBeer = commentOnBeer;
    }

    public List<String> getApprovalSeekingExamples() {
        return approvalSeekingExamples;
    }

    public void setApprovalSeekingExamples(List<String> approvalSeekingExamples) {
        this.approvalSeekingExamples = approvalSeekingExamples;
    }

    public String getDifference() {
        return difference;
    }

    public void setDifference(String difference) {
        this.difference = difference;
    }

    public String getOpposingConsequences() {
        return opposingConsequences;
    }

    public void setOpposingConsequences(String opposingConsequences) {
        this.opposingConsequences = opposingConsequences;
    }

    public String getPossibleConsequence() {
        return possibleConsequence;
    }

    public void setPossibleConsequence(String possibleConsequence) {
        this.possibleConsequence = possibleConsequence;
    }

    public String getAlternativeProcess() {
        return alternativeProcess;
    }

    public void setAlternativeProcess(String alternativeProcess) {
        this.alternativeProcess = alternativeProcess;
    }

    public String getFeelings() {
        return feelings;
    }

    public void setFeelings(String feelings) {
        this.feelings = feelings;
    }

    public String getLearningOutcome() {
        return learningOutcome;
    }

    public void setLearningOutcome(String learningOutcome) {
        this.learningOutcome = learningOutcome;
    }

    public String getCommentOnIssue_1() {
        return commentOnIssue_1;
    }

    public void setCommentOnIssue_1(String commentOnIssue_1) {
        this.commentOnIssue_1 = commentOnIssue_1;
    }

    public String getCommentOnIssue_2() {
        return commentOnIssue_2;
    }

    public void setCommentOnIssue_2(String commentOnIssue_2) {
        this.commentOnIssue_2 = commentOnIssue_2;
    }

    public String getCommentOnIssue_3() {
        return commentOnIssue_3;
    }

    public void setCommentOnIssue_3(String commentOnIssue_3) {
        this.commentOnIssue_3 = commentOnIssue_3;
    }

    public String getCommentOnIssue_4() {
        return commentOnIssue_4;
    }

    public void setCommentOnIssue_4(String commentOnIssue_4) {
        this.commentOnIssue_4 = commentOnIssue_4;
    }


}
