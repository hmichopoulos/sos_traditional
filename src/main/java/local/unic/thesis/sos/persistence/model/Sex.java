package local.unic.thesis.sos.persistence.model;

public enum Sex {
    MALE,
    FEMALE
}
