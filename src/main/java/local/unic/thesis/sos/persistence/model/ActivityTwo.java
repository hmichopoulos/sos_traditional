package local.unic.thesis.sos.persistence.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class ActivityTwo extends AbstractActivity implements Serializable {

    @Column
    private String definition;

    @Column
    private String purpose;

    @Column
    private String target;

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
