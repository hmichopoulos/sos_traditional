package local.unic.thesis.sos.persistence.repository;

import local.unic.thesis.sos.persistence.model.ActivityOne;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ActivityOneRepository extends CrudRepository<ActivityOne, Long> {

    ActivityOne findOneByUserUsername(String username);

    ActivityOne findOneByUserId(long userId);

    List<ActivityOne> findFirst5ByUserUsernameNot(String username);

    @Query("select ae from ActivityOne as ae where ae.user.username <> ?1 and ae.choicesAndConsequences is not null and ae.decisionExample is not null")
    Page<ActivityOne> findFirst5OtherOpinions(String username, Pageable pageable);

    @Query("select ae from ActivityOne as ae where ae.user.username <> ?1 " +
            " and ae.decisionMakeAlternatives is not null " +
            " and ae.decisionMakeHow is not null " +
            " and ae.decisionMakeProsCons is not null ")
    Page<ActivityOne> findFirst5OtherProcesses(String username, Pageable pageRequest);

    @Query("select ae from ActivityOne as ae where ae.user.username <> ?1 and ae.activity.imagePath is not null ")
    Page<ActivityOne> findFirst5OtherFiles(String username, Pageable pageRequest);
}
