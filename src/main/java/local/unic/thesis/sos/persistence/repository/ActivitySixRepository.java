package local.unic.thesis.sos.persistence.repository;

import local.unic.thesis.sos.persistence.model.ActivitySix;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ActivitySixRepository extends JpaRepository<ActivitySix, Long> {

    ActivitySix findOneByUserUsername(String username);

    ActivitySix findOneByUserId(long userId);

    @Query("select d.text from ActivitySix as s inner join s.decisions as d where s.user.username <> ?1")
    List<String> findFirst10OthersDecisions(String username);
}
