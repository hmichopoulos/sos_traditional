package local.unic.thesis.sos.persistence.model;

import javax.persistence.Embeddable;

@Embeddable
public class ActivityThreeObstacle {

    private String description;

    private String availableHelp;

    public ActivityThreeObstacle() {
    }

    public ActivityThreeObstacle(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAvailableHelp() {
        return availableHelp;
    }

    public void setAvailableHelp(String availableHelp) {
        this.availableHelp = availableHelp;
    }
}
