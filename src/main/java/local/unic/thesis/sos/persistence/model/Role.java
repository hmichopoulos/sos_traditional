package local.unic.thesis.sos.persistence.model;

public enum Role {
    ADMIN,
    USER;
}
