package local.unic.thesis.sos.persistence.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ActivitySix extends AbstractActivity implements Serializable {

    @Column
    private String decisionNeed;

    @Column
    private String noDecision;

    @Column
    private String twoPersons;

    @Column
    private String knowledgeBenefit;

    @Column
    private String alternatives;

    @ElementCollection
    @CollectionTable(name="activity_six_decisions", joinColumns=@JoinColumn(name="id"))
    private List<ActivitySixDecision> decisions = new ArrayList<>();

    public String getDecisionNeed() {
        return decisionNeed;
    }

    public void setDecisionNeed(String decisionNeed) {
        this.decisionNeed = decisionNeed;
    }

    public String getNoDecision() {
        return noDecision;
    }

    public void setNoDecision(String noDecision) {
        this.noDecision = noDecision;
    }

    public String getTwoPersons() {
        return twoPersons;
    }

    public void setTwoPersons(String twoPersons) {
        this.twoPersons = twoPersons;
    }

    public String getKnowledgeBenefit() {
        return knowledgeBenefit;
    }

    public void setKnowledgeBenefit(String knowledgeBenefit) {
        this.knowledgeBenefit = knowledgeBenefit;
    }

    public String getAlternatives() {
        return alternatives;
    }

    public void setAlternatives(String alternatives) {
        this.alternatives = alternatives;
    }

    public List<ActivitySixDecision> getDecisions() {
        return decisions;
    }

    public void setDecisions(List<ActivitySixDecision> decisions) {
        this.decisions = decisions;
    }
}
