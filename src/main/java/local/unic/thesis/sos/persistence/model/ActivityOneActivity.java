package local.unic.thesis.sos.persistence.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

@Embeddable
public class ActivityOneActivity implements Serializable {

    @Enumerated(EnumType.STRING)
    private ActivityOneChoice activityChoice;

    @Column
    private String imagePath;

    public ActivityOneChoice getActivityChoice() {
        return activityChoice;
    }

    public void setActivityChoice(ActivityOneChoice activityChoice) {
        this.activityChoice = activityChoice;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
