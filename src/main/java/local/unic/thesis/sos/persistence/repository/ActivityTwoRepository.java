package local.unic.thesis.sos.persistence.repository;

import local.unic.thesis.sos.persistence.model.ActivityTwo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ActivityTwoRepository extends CrudRepository<ActivityTwo, Long> {

    ActivityTwo findOneByUserUsername(String username);

    ActivityTwo findOneByUserId(long userId);

    List<ActivityTwo> findFirst5ByUserUsernameNot(String username);

    @Query("select a from ActivityTwo a where a.user.username <> ?1 " +
            " and a.definition is not null " +
            " and a.purpose is not null")
    Page<ActivityTwo> findFirst5OtherOpinions(String username, Pageable pageable);
}
