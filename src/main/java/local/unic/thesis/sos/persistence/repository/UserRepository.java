package local.unic.thesis.sos.persistence.repository;

import local.unic.thesis.sos.persistence.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findOneByUsername(String username);
}
