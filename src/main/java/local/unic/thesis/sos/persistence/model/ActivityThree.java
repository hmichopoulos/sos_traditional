package local.unic.thesis.sos.persistence.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class ActivityThree extends AbstractActivity implements Serializable {

    @ElementCollection
    @CollectionTable(name="activity_three_obstacle", joinColumns=@JoinColumn(name="id"))
    private List<ActivityThreeObstacle> obstacles = new ArrayList<>();

    @Column
    private String improvement;

    @Column
    private String availableHelp;

    @Column
    private String pastHelp;

    @Column
    private String target;

    @Column
    private String steps;

    @Column
    private Date deadline;

    public List<ActivityThreeObstacle> getObstacles() {
        return obstacles;
    }

    public void setObstacles(List<ActivityThreeObstacle> obstacles) {
        this.obstacles = obstacles;
    }

    public String getImprovement() {
        return improvement;
    }

    public void setImprovement(String improvement) {
        this.improvement = improvement;
    }

    public String getAvailableHelp() {
        return availableHelp;
    }

    public void setAvailableHelp(String availableHelp) {
        this.availableHelp = availableHelp;
    }

    public String getPastHelp() {
        return pastHelp;
    }

    public void setPastHelp(String pastHelp) {
        this.pastHelp = pastHelp;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }
}
