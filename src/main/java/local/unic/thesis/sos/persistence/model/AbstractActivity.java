package local.unic.thesis.sos.persistence.model;

import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
public class AbstractActivity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(optional = false)
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
