package local.unic.thesis.sos.persistence.model;

public enum ActivityOneChoice {
    SOMEONE_CALLS_YOU_A_NAME("Κάποιος σε προσφωνεί προσβλητικά"),
    DISOBEY_THE_REFEREE("Παρακούς το διαιτητή"),
    SOMEBODY_SHARES_THEIR_CAKE_WITH_YOU("Κάποιος μοιράζεται μαζί σου το γλυκό του"),
    PAY_A_COMPLIMENT("Κάνεις ένα κοπλιμέντο σε κάποιον"),
    DRINK_TOO_MUCH_COLA("Κάποιος πίνει πάρα πολύ κοκα κόλα"),
    WRITE_ON_THE_WALL_OF_THE_TOILET("Κάποιος γράφει στον τοίχο τής τουαλέτας με μαρκαδόρο"),
    START_FIRE_PLAYING_WITH_MATCHES("Κάποιος βάζει φωτιά παίζοντας με τα σπίρτα"),
    HELP_FOLD_THE_CLOTHES("Βοηθάς στο δίπλωμα των ρούχων"),
    MAKE_YOUR_BED_WITHOUT_BEING_TOLD("Στρώνεις το κρεβάτι σου χωρίς να στο πουν"),
    FORGOT_TO_FEED_YOUR_PET("Ξέχασες να ταΐσεις το κατοικίδιό σου"),
    IN_RAIN_WITHOUT_COVER("Βγαίνεις στη βροχή χωρίς να φοράς παλτό"),
    WAKE_UP_LATE_FOR_SCHOOL("Αργείς να ξυπνήσεις ένα πρωί για το σχολείο"),
    FORGOT_HOMEWORK_AT_HOME("Ξέχασες σπίτι την εργασία σου"),
    RETURN_HOME_LATE("Δεν προσέχεις την ώρα και γυρνάς σπίτι σου αργά"),
    PUSHED_OUT_OF_ORDER("Κάποιος σε σπρώχνει ενώ είσαι στη γραμμή (ουρά) στο κυλικείο"),
    FORGOT_YOUR_MONEY_AT_HOME("Ξέχασες το χαρτζιλίκι σου σπίτι"),
    DIDNT_DO_YOUR_HOMEWORK("Δεν έκανες την εργασία σου που ήταν για σήμερα"),
    LIE_TO_YOUR_PARENTS("Λες ένα ψέμα στους γονείς σου"),
    INTRODUCE_A_CLASSMATE_TO_YOUR_FRIENDS("Συστήνεις έναν συμμαθητή σου στους φίλους σου");

    private String description;

    private ActivityOneChoice(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
