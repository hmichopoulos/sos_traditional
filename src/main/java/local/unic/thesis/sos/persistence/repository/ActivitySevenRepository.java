package local.unic.thesis.sos.persistence.repository;

import local.unic.thesis.sos.persistence.model.ActivitySeven;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ActivitySevenRepository  extends CrudRepository<ActivitySeven, Long> {

    ActivitySeven findOneByUserUsername(String username);

    ActivitySeven findOneByUserId(long userId);

    List<ActivitySeven> findFirst5ByUserUsernameNot(String username);

    @Query("select a from ActivitySeven a where a.user.username <> ?1 and a.alternative is not null " +
            " and (a.advantages is not null or a.disadvantages is not null )")
    Page<ActivitySeven> findOthersAlternativeInfo(String username, Pageable pageable);

    Page<ActivitySeven> findByUserUsernameNotAndHypotheticalCaseNotNull(String username, Pageable pageable);

    Page<ActivitySeven> findByUserUsernameNotAndHelpGainedNotNull(String username, Pageable pageable);

    Page<ActivitySeven> findByUserUsernameNotAndThinkAlternativesNotNull(String username, Pageable pageable);

    Page<ActivitySeven> findByUserUsernameNotAndTargetPurposeNotNull(String username, Pageable pageable);
}
