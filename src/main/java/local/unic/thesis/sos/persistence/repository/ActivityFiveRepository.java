package local.unic.thesis.sos.persistence.repository;

import local.unic.thesis.sos.persistence.model.ActivityFive;
import org.springframework.data.repository.CrudRepository;

public interface ActivityFiveRepository extends CrudRepository<ActivityFive, Long> {

    ActivityFive findOneByUserUsername(String username);

    ActivityFive findOneByUserId(long userId);
}
