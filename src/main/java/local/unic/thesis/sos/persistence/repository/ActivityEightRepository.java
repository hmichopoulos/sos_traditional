package local.unic.thesis.sos.persistence.repository;

import local.unic.thesis.sos.persistence.model.ActivityEight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ActivityEightRepository extends JpaRepository<ActivityEight, Long> {

    ActivityEight findOneByUserUsername(String username);

    ActivityEight findOneByUserId(long userId);

    List<ActivityEight> findFirst5ByUserUsernameNot(String username);

    @Query("select e from ActivityEight as ae inner join ae.approvalSeekingExamples as e where ae.user.username != ?1")
    List<String> findFirst10OtherApprovalSeekingExamples(String username);

}
