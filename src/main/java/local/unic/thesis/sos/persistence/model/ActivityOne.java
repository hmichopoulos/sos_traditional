package local.unic.thesis.sos.persistence.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class ActivityOne extends AbstractActivity implements Serializable {

    @Column
    private Boolean waterSpill;

    @Column
    private Boolean waterGetWet;

    @Column
    private Boolean waterNothing;

    @Column
    private Boolean waterBreakGlass;

    @Column
    private Boolean lateAbsent;

    @Column
    private Boolean lateBadMark;

    @Column
    private Boolean lateNothing;

    @Column
    private Boolean submissionBadNote;

    @Column
    private Boolean submissionBadMark;

    @Column
    private Boolean submissionNothing;

    @Column
    private Boolean onBreakBadNote;

    @Column
    private Boolean onBreakLooseBreak;

    @Column
    private Boolean onBreakNothing;

    @Column
    private Boolean lateEscape;

    @Column
    private String choicesAndConsequences;

    @Column
    private String decisionExample;

    @Column
    private String decisionMakeHow;

    @Column
    private String decisionMakeAlternatives;

    @Column
    private String decisionMakeProsCons;

    @Embedded
    private ActivityOneActivity activity;

    public Boolean getWaterSpill() {
        return waterSpill;
    }

    public void setWaterSpill(Boolean waterSpill) {
        this.waterSpill = waterSpill;
    }

    public Boolean getWaterGetWet() {
        return waterGetWet;
    }

    public void setWaterGetWet(Boolean waterGetWet) {
        this.waterGetWet = waterGetWet;
    }

    public Boolean getWaterNothing() {
        return waterNothing;
    }

    public void setWaterNothing(Boolean waterNothing) {
        this.waterNothing = waterNothing;
    }

    public Boolean getWaterBreakGlass() {
        return waterBreakGlass;
    }

    public void setWaterBreakGlass(Boolean waterBreakGlass) {
        this.waterBreakGlass = waterBreakGlass;
    }

    public Boolean getLateAbsent() {
        return lateAbsent;
    }

    public void setLateAbsent(Boolean lateAbsent) {
        this.lateAbsent = lateAbsent;
    }

    public Boolean getLateBadMark() {
        return lateBadMark;
    }

    public void setLateBadMark(Boolean lateBadMark) {
        this.lateBadMark = lateBadMark;
    }

    public Boolean getLateNothing() {
        return lateNothing;
    }

    public void setLateNothing(Boolean lateNothing) {
        this.lateNothing = lateNothing;
    }

    public Boolean getLateEscape() {
        return lateEscape;
    }

    public void setLateEscape(Boolean lateEscape) {
        this.lateEscape = lateEscape;
    }

    public Boolean getSubmissionBadNote() {
        return submissionBadNote;
    }

    public void setSubmissionBadNote(Boolean submissionBadNote) {
        this.submissionBadNote = submissionBadNote;
    }

    public Boolean getSubmissionBadMark() {
        return submissionBadMark;
    }

    public void setSubmissionBadMark(Boolean submissionBadMark) {
        this.submissionBadMark = submissionBadMark;
    }

    public Boolean getSubmissionNothing() {
        return submissionNothing;
    }

    public void setSubmissionNothing(Boolean submissionNothing) {
        this.submissionNothing = submissionNothing;
    }

    public Boolean getOnBreakBadNote() {
        return onBreakBadNote;
    }

    public void setOnBreakBadNote(Boolean onBreakBadNote) {
        this.onBreakBadNote = onBreakBadNote;
    }

    public Boolean getOnBreakLooseBreak() {
        return onBreakLooseBreak;
    }

    public void setOnBreakLooseBreak(Boolean onBreakLooseBreak) {
        this.onBreakLooseBreak = onBreakLooseBreak;
    }

    public Boolean getOnBreakNothing() {
        return onBreakNothing;
    }

    public void setOnBreakNothing(Boolean onBreakNothing) {
        this.onBreakNothing = onBreakNothing;
    }

    public String getChoicesAndConsequences() {
        return choicesAndConsequences;
    }

    public void setChoicesAndConsequences(String choicesAndConsequences) {
        this.choicesAndConsequences = choicesAndConsequences;
    }

    public String getDecisionExample() {
        return decisionExample;
    }

    public void setDecisionExample(String decisionExample) {
        this.decisionExample = decisionExample;
    }

    public String getDecisionMakeHow() {
        return decisionMakeHow;
    }

    public void setDecisionMakeHow(String decisionMakeHow) {
        this.decisionMakeHow = decisionMakeHow;
    }

    public String getDecisionMakeAlternatives() {
        return decisionMakeAlternatives;
    }

    public void setDecisionMakeAlternatives(String decisionMakeAlternatives) {
        this.decisionMakeAlternatives = decisionMakeAlternatives;
    }

    public String getDecisionMakeProsCons() {
        return decisionMakeProsCons;
    }

    public void setDecisionMakeProsCons(String decisionMakeProsCons) {
        this.decisionMakeProsCons = decisionMakeProsCons;
    }

    public ActivityOneActivity getActivity() {
        return activity;
    }

    public void setActivity(ActivityOneActivity activity) {
        this.activity = activity;
    }
}