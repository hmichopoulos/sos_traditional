package local.unic.thesis.sos.persistence.repository;

import local.unic.thesis.sos.persistence.model.ActivityThree;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ActivityThreeRepository extends JpaRepository<ActivityThree, Long> {

    ActivityThree findOneByUserUsername(String username);

    ActivityThree findOneByUserId(long userId);

    List<ActivityThree> findFirst5ByUserUsernameNot(String username);

    @Query("select a from ActivityThree a where a.user.username <> ?1 " +
            " and a.target is not null " +
            " and a.improvement is not null " +
            " and a.availableHelp is not null ")
    Page<ActivityThree> findFirst5OthersExamples(String username, Pageable pageRequest);
}
