package local.unic.thesis.sos.persistence.model;

import javax.persistence.Embeddable;

@Embeddable
public class ActivitySixDecision {

    private String text;

    private Long rating;

    public ActivitySixDecision() {
    }

    public ActivitySixDecision(String text) {
        this.text = text;
    }

    public ActivitySixDecision(String text, Long rating) {
        this.text = text;
        this.rating = rating;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }
}
