package local.unic.thesis.sos.persistence.repository;

import local.unic.thesis.sos.persistence.model.ActivityFour;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ActivityFourRepository extends CrudRepository<ActivityFour, Long> {

    ActivityFour findOneByUserUsername(String username);

    ActivityFour findOneByUserId(long userId);

    @Query("select a from ActivityFour a where a.user.username <> ?1 " +
            " and a.step_1 is not null ")
    Page<ActivityFour> findOthersStep1(String username, Pageable pageable);

    @Query("select a from ActivityFour a where a.user.username <> ?1 " +
            " and a.step_2 is not null ")
    Page<ActivityFour> findOthersStep2(String username, Pageable pageable);

    @Query("select a from ActivityFour a where a.user.username <> ?1 " +
            " and a.extraInfo is not null " +
            " and a.validateInfo is not null ")
    Page<ActivityFour> findOthersStep3(String username, Pageable pageable);

    @Query("select a from ActivityFour a where a.user.username <> ?1 " +
            " and ( a.noUniversity is not null " +
            " or a.noMarriage is not null " +
            " or a.noSchool is not null " +
            " or a.drugs is not null " +
            " or a.becomeSinger is not null " +
            " or a.noAlcohol is not null " +
            " or a.noKids is not null " +
            " or a.rationale is not null )")
    Page<ActivityFour> findOthersStep4(String username, Pageable pageable);

    @Query("select a from ActivityFour a where a.user.username <> ?1 " +
            " and ( a.decision is not null " +
            " or a.advantages is not null " +
            " or a.disadvantages is not null ) ")
    Page<ActivityFour> findOthersStep5(String username, Pageable pageable);

    @Query("select a from ActivityFour a where a.user.username <> ?1 " +
            " and a.isItEasy is not null " +
            " and a.difficulties is not null ")
    Page<ActivityFour> findOthersStep6(String username, Pageable pageable);

    @Query("select a from ActivityFour a where a.user.username <> ?1 " +
            " and a.step_7 is not null ")
    Page<ActivityFour> findOthersStep7(String username, Pageable pageable);
}
