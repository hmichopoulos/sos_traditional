package local.unic.thesis.sos.persistence.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class ActivitySeven extends AbstractActivity implements Serializable {

    @Column
    private String alternative;

    @Column
    private String advantages;

    @Column
    private String disadvantages;

    @Column
    private String hypotheticalCase;

    @Column
    private String hypotheticalStep_1;

    @Column
    private String hypotheticalStep_2;

    @Column
    private String hypotheticalStep_3;

    @Column
    private String hypotheticalStep_4;

    @Column
    private String hypotheticalStep_5;

    @Column
    private String hypotheticalStep_6;

    @Column
    private String hypotheticalStep_7;

    @Column
    private String helpGained;

    @Column
    private String thinkAlternatives;

    @Column
    private String targetPurpose;

    public String getAlternative() {
        return alternative;
    }

    public void setAlternative(String alternative) {
        this.alternative = alternative;
    }

    public String getAdvantages() {
        return advantages;
    }

    public void setAdvantages(String advantages) {
        this.advantages = advantages;
    }

    public String getDisadvantages() {
        return disadvantages;
    }

    public void setDisadvantages(String disadvantages) {
        this.disadvantages = disadvantages;
    }

    public String getHypotheticalCase() {
        return hypotheticalCase;
    }

    public void setHypotheticalCase(String hypotheticalCase) {
        this.hypotheticalCase = hypotheticalCase;
    }

    public String getHypotheticalStep_1() {
        return hypotheticalStep_1;
    }

    public void setHypotheticalStep_1(String hypotheticalStep_1) {
        this.hypotheticalStep_1 = hypotheticalStep_1;
    }

    public String getHypotheticalStep_2() {
        return hypotheticalStep_2;
    }

    public void setHypotheticalStep_2(String hypotheticalStep_2) {
        this.hypotheticalStep_2 = hypotheticalStep_2;
    }

    public String getHypotheticalStep_3() {
        return hypotheticalStep_3;
    }

    public void setHypotheticalStep_3(String hypotheticalStep_3) {
        this.hypotheticalStep_3 = hypotheticalStep_3;
    }

    public String getHypotheticalStep_4() {
        return hypotheticalStep_4;
    }

    public void setHypotheticalStep_4(String hypotheticalStep_4) {
        this.hypotheticalStep_4 = hypotheticalStep_4;
    }

    public String getHypotheticalStep_5() {
        return hypotheticalStep_5;
    }

    public void setHypotheticalStep_5(String hypotheticalStep_5) {
        this.hypotheticalStep_5 = hypotheticalStep_5;
    }

    public String getHypotheticalStep_6() {
        return hypotheticalStep_6;
    }

    public void setHypotheticalStep_6(String hypotheticalStep_6) {
        this.hypotheticalStep_6 = hypotheticalStep_6;
    }

    public String getHypotheticalStep_7() {
        return hypotheticalStep_7;
    }

    public void setHypotheticalStep_7(String hypotheticalStep_7) {
        this.hypotheticalStep_7 = hypotheticalStep_7;
    }

    public String getHelpGained() {
        return helpGained;
    }

    public void setHelpGained(String helpGained) {
        this.helpGained = helpGained;
    }

    public String getThinkAlternatives() {
        return thinkAlternatives;
    }

    public void setThinkAlternatives(String thinkAlternatives) {
        this.thinkAlternatives = thinkAlternatives;
    }

    public String getTargetPurpose() {
        return targetPurpose;
    }

    public void setTargetPurpose(String targetPurpose) {
        this.targetPurpose = targetPurpose;
    }
}
