package local.unic.thesis.sos.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class ActivityFour extends AbstractActivity implements Serializable {

    @Column
    private String step_1;

    @Column
    private String step_2;

    @Column
    private String extraInfo;
    @Column
    private String validateInfo;

    @Column
    private String noUniversity;
    @Column
    private String noMarriage;
    @Column
    private String noSchool;
    @Column
    private String drugs;
    @Column
    private String becomeSinger;
    @Column
    private String noAlcohol;
    @Column
    private String noKids;
    @Column
    private String rationale;

    @Column
    private String decision;
    @Column
    private String advantages;
    @Column
    private String disadvantages;

    @Column
    private String isItEasy;
    @Column
    private String difficulties;

    @Column
    private String step_7;

    public ActivityFour() {
    }

    public String getStep_1() {
        return step_1;
    }

    public void setStep_1(String step_1) {
        this.step_1 = step_1;
    }

    public String getStep_2() {
        return step_2;
    }

    public void setStep_2(String step_2) {
        this.step_2 = step_2;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    public String getValidateInfo() {
        return validateInfo;
    }

    public void setValidateInfo(String validateInfo) {
        this.validateInfo = validateInfo;
    }

    public String getNoUniversity() {
        return noUniversity;
    }

    public void setNoUniversity(String noUniversity) {
        this.noUniversity = noUniversity;
    }

    public String getNoMarriage() {
        return noMarriage;
    }

    public void setNoMarriage(String noMarriage) {
        this.noMarriage = noMarriage;
    }

    public String getNoSchool() {
        return noSchool;
    }

    public void setNoSchool(String noSchool) {
        this.noSchool = noSchool;
    }

    public String getDrugs() {
        return drugs;
    }

    public void setDrugs(String drugs) {
        this.drugs = drugs;
    }

    public String getBecomeSinger() {
        return becomeSinger;
    }

    public void setBecomeSinger(String becomeSinger) {
        this.becomeSinger = becomeSinger;
    }

    public String getNoAlcohol() {
        return noAlcohol;
    }

    public void setNoAlcohol(String noAlcohol) {
        this.noAlcohol = noAlcohol;
    }

    public String getNoKids() {
        return noKids;
    }

    public void setNoKids(String noKids) {
        this.noKids = noKids;
    }

    public String getRationale() {
        return rationale;
    }

    public void setRationale(String rationale) {
        this.rationale = rationale;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public String getAdvantages() {
        return advantages;
    }

    public void setAdvantages(String advantages) {
        this.advantages = advantages;
    }

    public String getDisadvantages() {
        return disadvantages;
    }

    public void setDisadvantages(String disadvantages) {
        this.disadvantages = disadvantages;
    }

    public String getIsItEasy() {
        return isItEasy;
    }

    public void setIsItEasy(String isItEasy) {
        this.isItEasy = isItEasy;
    }

    public String getDifficulties() {
        return difficulties;
    }

    public void setDifficulties(String difficulties) {
        this.difficulties = difficulties;
    }

    public String getStep_7() {
        return step_7;
    }

    public void setStep_7(String step_7) {
        this.step_7 = step_7;
    }
}
