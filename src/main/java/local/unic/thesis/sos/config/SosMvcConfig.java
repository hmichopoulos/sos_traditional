package local.unic.thesis.sos.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.io.File;
import java.io.IOException;

@Configuration
public class SosMvcConfig extends WebMvcConfigurerAdapter {

    @Value("${app.imageLocation}")
    String imageStorageLocation;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //super.addViewControllers(registry);
        registry.addViewController("/login").setViewName("login");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        super.addResourceHandlers(registry);
        try {
            registry.addResourceHandler("/dynamic-image/**")
                    .addResourceLocations("file:///" + new File(imageStorageLocation).getCanonicalPath().replace("\\", "/") + "/");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
