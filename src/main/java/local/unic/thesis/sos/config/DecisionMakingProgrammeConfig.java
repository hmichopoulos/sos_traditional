package local.unic.thesis.sos.config;

import local.unic.thesis.sos.ui.model.Lesson;
import local.unic.thesis.sos.ui.model.Module;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class DecisionMakingProgrammeConfig {

    private List<Module> activityOneModules() {
        List<Module> modules = new ArrayList<>();

        modules.add(new Module("Παρουσίαση", 0, 1));
        modules.add(new Module("Κουίζ", 1, 5));
        modules.add(new Module("Λίγα λόγια", 2 ,4));
        modules.add(new Module("Η γνώμη σου", 3, 5));
        modules.add(new Module("Μια τεχνική", 4, 11));
        modules.add(new Module("Δραστηριότητα", 5, 4));

        return modules;
    }

    public List<Module> activityTwoModules() {
        List<Module> modules = new ArrayList<>();

        modules.add(new Module("Εισαγωγή", 0, 4));
        modules.add(new Module("Συζήτηση", 1, 2));
        modules.add(new Module("Δραστηριότητα", 2, 2));

        return modules;
    }

    public List<Module> activityThreeModules() {
        List<Module> modules = new ArrayList<>();

        modules.add(new Module("Εισαγωγή", 0, 1));
        modules.add(new Module("Σύντομη συζήτηση", 1, 2));
        modules.add(new Module("Δραστηριότητα", 2 ,3));
        modules.add(new Module("Συμβόλαιο επιτυχίας", 3, 1));

        return modules;
    }

    public List<Module> activityFourModules() {
        List<Module> modules = new ArrayList<>();

        modules.add(new Module("Σύντομη συζήτηση", 0, 1));
        modules.add(new Module("Δραστηριότητα", 1 ,16));

        return modules;
    }

    public List<Module> activityFiveModules() {
        List<Module> modules = new ArrayList<>();

        modules.add(new Module("Εισαγωγή", 0, 2));
        modules.add(new Module("Λίγα λόγια", 1, 8));
        modules.add(new Module("Συζήτηση", 2, 3));
        modules.add(new Module("Οι δραστηριότητές μου", 3, 1));

        return modules;
    }

    public List<Module> activitySixModules() {
        List<Module> modules = new ArrayList<>();

        modules.add(new Module("Εισαγωγή", 0, 1));
        modules.add(new Module("Λίγα λόγια", 1 ,7));
        modules.add(new Module("Συζήτηση", 2, 2));

        return modules;
    }

    public List<Module> activitySevenModules() {
        List<Module> modules = new ArrayList<>();

        modules.add(new Module("Παρουσίαση", 0, 1));
        modules.add(new Module("Η διαδικασία", 1, 3));
        modules.add(new Module("Δραστηριότητα", 2, 2));
        modules.add(new Module("Συζήτηση", 3, 7));

        return modules;
    }

    private List<Module> activityEightModules() {
        List<Module> modules = new ArrayList<>();

        modules.add(new Module("Εισαγωγή", 0, 1));
        modules.add(new Module("Σχολιασμός", 1 ,4));
        modules.add(new Module("Συζήτηση", 2, 6));
        modules.add(new Module("Δραστηριότητα", 3, 7));

        return modules;
    }

    public List<Module> activityNineModules() {
        List<Module> modules = new ArrayList<>();

        modules.add(new Module("Σύντομη συζήτηση", 0, 1));

        return modules;
    }

    @Bean(name = "decisionMakingLessons")
    public List<Lesson> decisionMakingLessons() {
        List<Lesson> lessons = new ArrayList<>();
        Lesson lesson;
        lessons.add(lesson = new Lesson("Συνέπειες επιλογών και αποφάσεων", "/activity/1/0-0"));
        lesson.setModules(activityOneModules());

        lessons.add(lesson = new Lesson("Ο τροχός του στόχου", "/activity/2/0-0"));
        lesson.setModules(activityTwoModules());

        lessons.add(lesson = new Lesson("Υπερπηδώντας τα εμπόδια", "/activity/3/0-0"));
        lesson.setModules(activityThreeModules());

        lessons.add(lesson = new Lesson("Αποφάσεις, Αποφάσεις!", "/activity/4/0-0"));
        lesson.setModules(activityFourModules());

        lessons.add(lesson = new Lesson("Θέτοντας και επιτυγχάνοντας στόχους", "/activity/5/0-0"));
        lesson.setModules(activityFiveModules());

        lessons.add(lesson = new Lesson("Έκβαση αποφάσεων", "/activity/6/0-0"));
        lesson.setModules(activitySixModules());

        lessons.add(lesson = new Lesson("Αποφάσεις, Αποφάσεις! (2)", "/activity/7/0-0"));
        lesson.setModules(activitySevenModules());

        lessons.add(lesson = new Lesson("Κοινωνική αποδοχή και συνέπειες", "/activity/8/0-0"));
        lesson.setModules(activityEightModules());

        lessons.add(lesson = new Lesson("Ξανακοιτώντας μια παλιά απόφαση", "/activity/9/0-0"));
        lesson.setModules(activityNineModules());

        return lessons;
    }
}
