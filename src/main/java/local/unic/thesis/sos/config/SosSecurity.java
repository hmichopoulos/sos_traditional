package local.unic.thesis.sos.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SosSecurity extends WebSecurityConfigurerAdapter {

    @Value("${flyway.enabled:true}")
    private boolean createTables;

    @Bean @Autowired
    JdbcTokenRepositoryImpl jdbcTokenRepository(DataSource dataSource) {
        JdbcTokenRepositoryImpl service = new JdbcTokenRepositoryImpl();
        service.setDataSource(dataSource);
        service.setCreateTableOnStartup(!createTables);
        return service;
    }

    @Autowired
    SosUserDetailService sosUserDetailService;

    @Autowired
    PersistentTokenRepository persistentTokenRepository;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //super.configure(http);
        http
            .csrf()
                .disable()
            .authorizeRequests()
                .antMatchers("/", "/home", "/register", "/api/**").permitAll()
                .antMatchers("/admin/**").hasAuthority("ADMIN")
                .anyRequest().authenticated()
            .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
             .and()
                .logout().deleteCookies("remember-me").logoutSuccessUrl("/").permitAll()
            .and()
                .rememberMe().tokenRepository(persistentTokenRepository);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(sosUserDetailService);
    }
}
