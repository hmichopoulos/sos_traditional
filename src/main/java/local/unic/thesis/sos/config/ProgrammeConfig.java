package local.unic.thesis.sos.config;

import local.unic.thesis.sos.ui.model.Programme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class ProgrammeConfig {

    @Bean(name = "theProgrammes")
    public List<Programme> theProgrammes() {
        List<Programme> list = new ArrayList<>();
        list.add(new Programme(
                "Λαμβάνω αποφάσεις",
                "Δραστηριότητες για να γνωρίσεις καλύτερα τη διαδικασία λήψης αποφάσεων, να γνωρίσεις τεχνικές και να τις εφαρμόσεις ώστε να παίρνεις αποφάσεις με καλύτερες πιθανότητες επιτυχίας.",
                "decision-making.jpg",
                "/decision-making",
                true
        ));

        list.add(new Programme(
                "Επίλυση συγκρούσεων",
                "Οι συγκρούσεις και οι αντιπαραθέσεις είναι κομμάτι τής ζωής. Οι δραστηριότητες αυτής της ομάδας θα σε βοηθήσουν να γνωρίσεις τεχνικές ώστε να διαχειρίζαι καλύτερα τις διενέξεις που θα σου τυχαίνουν.",
                "conflict-resolution.png",
                "activity-one.1.0",
                false
        ));

        list.add(new Programme(
                "Συμπεριφέρομαι υπεύθυνα",
                "Responsible behavior.",
                "responsible-behavior.png",
                "activity-one.1.0",
                false
        ));

        list.add(new Programme(
                "Αυτογνωσία και αποδοχή",
                "Responsible behavior..",
                "self-knowledge.jpg",
                "activity-one.1.0",
                false
        ));

        list.add(new Programme(
                "Φροντίζω τον εαυτό μου",
                "Responsible behavior..",
                "self-care.gif",
                "activity-one.1.0",
                false
        ));

        return list;
    }
}
